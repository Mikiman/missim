﻿using UnityEngine;
using System.Collections;

public class VectorTesting : MonoBehaviour {

	public Transform Loop;
	public Transform Peg;

	public Vector3 LoopForwardVector;
	public Vector3 PegForwardVector;
	public Vector3 LoopOnXZPlaneVector;
	public Vector3 PegOnXZPlaneVector;

	public float Angle;

	public bool CorrectDirection;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Calculate ();
	}

	public void Calculate(){
		LoopForwardVector = Loop.forward;
		PegForwardVector = Peg.forward;
		LoopOnXZPlaneVector = new Vector3(LoopForwardVector.x,0f,LoopForwardVector.z);
		PegOnXZPlaneVector = new Vector3(PegForwardVector.x,0f,PegForwardVector.z);
		Angle = Vector3.Angle (LoopOnXZPlaneVector, PegOnXZPlaneVector);

		//Abs makes it be able to come form +-90º
		if (Mathf.Abs (Angle) < 90) {
			CorrectDirection = true;
		} else {
			CorrectDirection = false;
		}
	}
}
