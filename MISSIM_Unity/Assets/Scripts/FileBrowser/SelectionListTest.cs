﻿using UnityEngine;
using System.Collections;

//http://wiki.unity3d.com/index.php/ImprovedSelectionList

public class SelectionListTest : MonoBehaviour {

	int currentSelection;
	public GUISkin skinWithListStyle;
	string[] myList = new string[] {"First", "Second", "Third"};

	protected void OnGUI() {
		GUI.skin = skinWithListStyle;
		currentSelection = GUILayoutx.SelectionList(currentSelection, myList, DoubleClickItem);
	}

	protected void DoubleClickItem(int index) {
		Debug.Log("Clicked " + myList[index]);
	}
}