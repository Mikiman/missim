﻿using UnityEngine;
using MyDataClasses;

public class TextFileFinder : MonoBehaviour {


	//public string Extension = Extensions.json;
	public ExtensionsEnum selectionPaternExtension,saveLoadExtension;

	protected FileBrowser m_fileBrowser;

	[SerializeField]
	protected Texture2D	m_directoryImage,
	m_fileImage;

	public  string m_textPath;


	protected void OnGUI () {
		if (m_fileBrowser != null) {
			m_fileBrowser.OnGUI();
		} else {
			OnGUIMain();
		}
	}

	protected void OnGUIMain() {

		GUILayout.BeginHorizontal();
		GUILayout.Label("Text File", GUILayout.Width(100));
		GUILayout.FlexibleSpace();
		GUILayout.Label(m_textPath ?? "none selected");
		if (GUILayout.Button("...", GUILayout.ExpandWidth(false))) {
			m_fileBrowser = new FileBrowser(
				new Rect(100, 100, 600, 500),
				"Choose Text File",
				FileSelectedCallback
			);
			m_fileBrowser.SelectionPattern = "*"+Extensions.getExtension(selectionPaternExtension);//"*.json";
			//m_fileBrowser.SelectionPattern = "*.json";

			m_fileBrowser.DirectoryImage = m_directoryImage;
			m_fileBrowser.FileImage = m_fileImage;
		}
		GUILayout.EndHorizontal();
	}

	protected void FileSelectedCallback(string path) {
		m_fileBrowser = null;
		m_textPath = path;
		myTesting (path);

	}


	protected void myTesting(string path){
		//JSON_Tools.SaveToJSONFile (new testSaveMeClass(path,Vector3.one*3),"/Resources/test.json");
		//FormatterTools.SaveToFile(new testSaveMeClass(path,Vector3.one*3),"/Resources/test.json");


		/*
		string json = FullSerializer_Tools.Serialize(typeof(testSaveMeClass),new testSaveMeClass(path,Vector3.one*3));
		Debug.Log ("JSON:" +json);

		FormatterTools.SaveToFile(json,"/Resources/test.txt");
		Invoke ("doLater", 5);
		*/
		MyComplexFormatter.SaveObjectToBinaryFile (new testSaveMeClass (path, Vector3.one * 3), "/Resources/test.txt", "");
		Invoke ("doLater", 5);

		//Debug.Log("JSON="+ MyComplexFormatter.SerializeToJSON(typeof(testSaveMeClass),new testSaveMeClass (path, Vector3.one * 8) ));
		testSaveMeClass test = new testSaveMeClass (path, Vector3.one * 8);
		string testString = MyComplexFormatter.SerializeToJSON (test);
		Debug.Log("To JSON="+ testString);
		testSaveMeClass testDeserialized = MyComplexFormatter.DeserializeFromJSON<testSaveMeClass> (testString);
		Debug.Log("From JSON="+ testDeserialized.myVector.ToString());

	}
	public void doLater(){
		/*
		 * string json = FormatterTools.LoadFromFile ("/Resources/test.txt");
		Debug.Log ("Loaded result:" + json);

		testSaveMeClass tsmc = (testSaveMeClass)FullSerializer_Tools.Deserialize (typeof(testSaveMeClass), json);
		Debug.Log ("Deserialized:" + tsmc.myVector.ToString());
		*/

		//testSaveMeClass test =  (testSaveMeClass)MyComplexFormatter.LoadObjectFromBinaryFile(typeof(testSaveMeClass),"/Resources/test.txt");
		testSaveMeClass test =  MyComplexFormatter.LoadObjectFromBinaryFile<testSaveMeClass>("/Resources/test.txt");
		Debug.Log ("Deserialized:" + test.myVector.ToString());


	}

}




[System.Serializable]
public class testSaveMeClass{
	public string path;
	public Vector3 myVector;
	public testSaveMeClass(string path,Vector3 vector){this.path = path;this.myVector = vector;}
	public testSaveMeClass(){this.path = "";this.myVector = Vector3.zero;}

}