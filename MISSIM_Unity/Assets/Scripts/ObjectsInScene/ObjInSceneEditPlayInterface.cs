﻿using UnityEngine;
using System.Collections;

public interface ObjInSceneEditPlayInterface  {

	void OnEnterPlayMode ();
	void OnEnterEditMode ();

}
