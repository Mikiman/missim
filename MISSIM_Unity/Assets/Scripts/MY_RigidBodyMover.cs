﻿using UnityEngine;
using System.Collections;

public class MY_RigidBodyMover : MonoBehaviour {
	public Vector3 Direction = new Vector3 (0f, 0f, 0f);
	private Rigidbody rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
	
	}


	public void ApplyForce(){
		rb.MovePosition (transform.position + Direction * Time.deltaTime);
	}


	// Update is called once per frame
	void Update () {
	
	}



}
