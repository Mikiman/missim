﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class SimpleClickedOnDetector : MonoBehaviour {

	public bool enabled=true;
	public EditorShell delegado; //call this script when clicked on me is detected.

	void OnMouseDown(){

		if (!enabled) {
			return;
		}

		//dont detect through ui ()I dont understand the !, but its needed
		if (!EventSystem.current.IsPointerOverGameObject()){
			
			//Debug.Log("Click On me Detected:" + gameObject.name);

			//USE THIS If As a COMPONENT
			//EventDemux.instance.ReportEventToDemux(MyEventMeanings.ClickedOnItemInScene, gameObject); //launch event

			//USE THIS If using delegados
			if (delegado != null) {
				delegado.ClickedOnMeInEditMode ();
			} else {
				Debug.LogError ("GameObject "+gameObject.name+" doesnt have a delegado in simpleclickedondetector.cs");
				//EventDemux.instance.ReportEventToDemux(MyEventMeanings.ClickedOnItemInScene, gameObject); //launch event
			}
			

		}


	}
}
