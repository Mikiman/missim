﻿using UnityEngine;
using System.Collections;

public class Loop : MonoBehaviour,ObjInSceneEditPlayInterface {

	Rigidbody rb;


	void OnTriggerEnter(Collider other){
		Peg peg = other.gameObject.GetComponentInParent<Peg> ();
		if (peg == null) {
			//Debug.Log ("Loop script is exiting beacuase There is no Peg script on other object");
			return;
		}
		if (peg.InLoop) {
			return;
		}
		//Debug.Log ("Peg entered loop");
		peg.InLoop = true;
		LoopTottem LT = GetComponentInParent<LoopTottem> ();

		if(LT==null){
			Debug.Log ("Loop script is malfunctioning beacuase There is no loop totem script on parent object");
			return;
		}
		LT.SetPeg (other.gameObject.transform.parent);
		LT.SetEntryPoint (other.transform.position);
	}




	void OnTriggerExit(Collider other){
		Peg peg = other.gameObject.GetComponentInParent<Peg> ();
		if (peg == null) {
			//Debug.Log ("Loop script is exiting beacuase There is no Peg script on other object");
			return;
		}
		//Debug.Log ("Peg exited loop");
		peg.InLoop = false;
			
		LoopTottem LT = GetComponentInParent<LoopTottem> ();

		if (LT == null) {
			Debug.Log ("Loop script is malfunctioning beacuase There is no loop totem script on parent object");
			return;
		}
		LT.SetExitPoint (other.transform.position);
		LT.Calculate ();


	}

	#region ObjInSceneEditPlayInterface implementation
	public void OnEnterPlayMode ()
	{
		if (rb == null) {
			rb = gameObject.GetComponent<Rigidbody> ();
		}

		if (rb != null) {
			rb.isKinematic=false;
		}

	}
	public void OnEnterEditMode ()
	{
		if (rb == null) {
			rb = gameObject.GetComponent<Rigidbody> ();
		}

		if (rb != null) {
			rb.isKinematic=true;
		}
	}
	#endregion
}
