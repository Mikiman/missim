﻿using UnityEngine;
using System.Collections;

public class PhysicsRotator : MonoBehaviour {

	//SEE: https://docs.unity3d.com/ScriptReference/Rigidbody.MoveRotation.html


	public Vector3 eulerAngleVelocity;
	public Rigidbody rb;
	public bool SimpleRotateContinuosly = false;

	public Vector3 MaxRotations = new Vector3 ();
	public const int Yaw = 2,Torsion = 1,Ptich =0;

	public float DebugFloat;
	

	void Start() {
		rb = GetComponent<Rigidbody>();
	}
	void FixedUpdate() {
		if (rb == null) {
			Debug.Log ("there is no RigidBody on the gameobject with the Physics Rotator Script");		
			return;
		}

		float z = rb.rotation.eulerAngles.z;
		DebugFloat = z;



		if (SimpleRotateContinuosly) {
			SimpleRotate ();
			return;
		} else {
			ClampAtMax ();
			SimpleRotate ();
		}


	}

	private void SimpleRotate(){
		//eulerAngleVelocity.z = Input.GetAxisRaw ("Horizontal");
		Quaternion deltaRotation = Quaternion.Euler(eulerAngleVelocity * Time.deltaTime);
		rb.MoveRotation(rb.rotation * deltaRotation);

	}



	/**
	 * Makes it be that it wont rotate more then the max value, both +-Max value
	*/
	public void ClampAtMax(){
		for(int i = 0;i<3;i++){
			float axisRotation = rb.rotation.eulerAngles [i];
			/*
			 * THIS DIDNT WORK WELL;BUT KIND OF WORKED
				bool a = MaxRotations[i] < axisRotation;	//outside of 0 and Max;
			bool clockwise = rb.velocity[i] <0;
			bool b = (360-axisRotation) <(360-MaxRotations[i]);

			if ((!clockwise && a) || (clockwise&&b)) {
				eulerAngleVelocity[i] *= -1;		//swings oopposite direction

			}
			//Debug.Log ("i = " + i + ", Rot= " + axisRotation + ", MaxRot = " + MaxRotations [i] + ", A:" + a + ", B:" + b); 
			*/


			//this puts 0 to 180, then -180 to 0
			if (axisRotation > 180) {
				axisRotation -= 360;
			}
			if (Mathf.Abs (axisRotation) > MaxRotations [i]+0.2) {	//the +0.2 helps it get unstuck from a precision based epsilon bug.
				//eulerAngleVelocity[i] *= -1;		//swings oopposite direction
				eulerAngleVelocity[i] = 0;		//stops it
			}



		}

	}

}
