﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SixSliderTransformModifier : MonoBehaviour {

	public Transform TargetTransform;

	public Slider PosXSlider;
	public Slider PosYSlider;
	public Slider PosZSlider;

	public Slider RotXSlider;
	public Slider RotYSlider;
	public Slider RotZSlider;


	[SerializeField]
	private Vector3 DefaultPosition;
	[SerializeField]
	private Vector3 DefaultRotation;
	[SerializeField]
	private float DefaultZoom;





	// Use this for initialization
	void Start () {
		if (TargetTransform == null) {
			return;
		}
		SetDefaultValues ();

	}

	// Update is called once per frame
	void Update () {

	}

	public void SetDefaultValues(){
		DefaultPosition = TargetTransform.position;
		DefaultRotation = TargetTransform.eulerAngles;
		DefaultRotation = RotationBetween180 (DefaultRotation);//These ifs fix a bug caused by maxValue = 180 and start =270 (=-90)
		setUpSliders ();
	}

	public void SliderValueChanged(){
		TargetTransform.position = new Vector3 (PosXSlider.value,PosYSlider.value,PosZSlider.value);
		TargetTransform.rotation = Quaternion.Euler (RotXSlider.value,RotYSlider.value,RotZSlider.value);
	}



	private Vector3 RotationBetween180(Vector3 rotation){
		//These ifs fix a bug caused by maxValue = 180 and start =270 (=-90)
		if (rotation.x > RotXSlider.maxValue) {
			rotation.x -= 360;
		}
		if (rotation.y > RotYSlider.maxValue) {
			rotation.y -= 360;
		}
		if (rotation.z > RotZSlider.maxValue) {
			rotation.z -= 360;
		}
		return rotation;
	}
	private void setUpSliders(){
		Vector3 Pos = TargetTransform.position;
		Vector3 Rot = TargetTransform.eulerAngles;

		PosXSlider.value = Pos.x;
		PosYSlider.value = Pos.y;
		PosZSlider.value = Pos.z;

		Rot = RotationBetween180 (Rot);
		RotXSlider.value = Rot.x;
		RotYSlider.value = Rot.y;
		RotZSlider.value = Rot.z;

	}

	public void ResetTransformToDefault(){
		TargetTransform.position = DefaultPosition;
		TargetTransform.eulerAngles = DefaultRotation;

		setUpSliders ();
	}

	public void SetTarget(Transform targ){
		TargetTransform = targ;
		SetDefaultValues ();
	}
}
