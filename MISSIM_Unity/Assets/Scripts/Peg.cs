﻿using UnityEngine;
using System.Collections;

public class Peg : MonoBehaviour,ObjInSceneEditPlayInterface {
	public bool InLoop;


	#region ObjInSceneEditPlayInterface implementation
	public void OnEnterPlayMode ()
	{
		gameObject.GetComponent<Rigidbody> ().isKinematic=false;

	}
	public void OnEnterEditMode ()
	{
		gameObject.GetComponent<Rigidbody> ().isKinematic=true;
	}
	#endregion
}
