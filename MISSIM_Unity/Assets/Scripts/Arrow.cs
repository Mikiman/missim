﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour {
	private Quaternion q;
	private Vector3 v3;
	private bool hasHit = false;
	Transform MyTransform;
	Rigidbody MyRigidbody;
	public bool StickInWallOrFallDown=true; //true: stick,  false: fall

	// Use this for initialization
	void Start () {            
		MyTransform = this.transform;
		MyRigidbody = this.GetComponent<Rigidbody>();
		MyRigidbody.AddForce(MyTransform.forward * 50, ForceMode.VelocityChange);            
	}

	void OnCollisionEnter(Collision col)
	{        
		MyRigidbody.isKinematic = true;    
		hasHit = true;
	}

	void LateUpdate() {
		if (hasHit) {
			transform.position = v3;
			transform.rotation = q;
			hasHit = false;

			//if FallDown, do this
			if (!StickInWallOrFallDown) {
				MyRigidbody.isKinematic = false;
			}
		}
		else {
			v3 = transform.position;
			q = transform.rotation;
		}
	}


}