﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class EditorShell :MonoBehaviour {

	//public GameObject objInScene;

	public Collider editModeCollider; //Box Collider that will surround the whole GO.
	public Collider[] playModeColliders; //colliders on the GO
	public Collider[] childrenColliders; //colliders in the GO children

	public ObjInSceneEditPlayInterface[] objInScene;


	SimpleClickedOnDetector clickDetector; //script that detects when the GO has been clicked on



	// Use this for initialization
	void Start () {
		SetupClickDetector ();
		childrenColliders = GetComponentsInChildren<Collider> (); 

		objInScene=GameObjectExtensions.GetInterfacesInChildren<ObjInSceneEditPlayInterface> (gameObject);
		//ToEditMode ();
	}


	/// <summary>
	/// Handles what needs to be done when switching from edit mode to Runtime (Playmode).
	/// </summary>
	public void ToPlayMode(){
		//turn on colliders on this object
		foreach (Collider c in playModeColliders) {
			c.enabled = true;
		}
		//turn on colliders in children
		foreach (Collider c in childrenColliders) {
			c.enabled = true;
		}

		//OJO CAREFUL: editModeCollider is included in childrenColliders. Make sure these 2 lines go after the foreach
		//Turn off edit mode stuff.
		editModeCollider.enabled = false;
		SetupClickDetector ();
		clickDetector.enabled = false;

		//do necesary things to the taskObject
		if (objInScene == null) {
			objInScene=GameObjectExtensions.GetInterfacesInChildren<ObjInSceneEditPlayInterface> (gameObject);
		}
		foreach (ObjInSceneEditPlayInterface obj in objInScene) {
			obj.OnEnterPlayMode ();
		}

			
	}


	/// <summary>
	/// Handles what needs to be done when switching from Runtime (Playmode) to the edit mode.
	/// </summary>
	public void ToEditMode(){
		//turn off colliders on this object
		foreach (Collider c in playModeColliders) {
			c.enabled = false;
		}
		//turn off colliders in children
		foreach (Collider c in childrenColliders) {
			c.enabled = false;
		}

		//OJO CAREFUL: editModeCollider is included in childrenColliders. Make sure these 2 lines go after the foreach
		//Turn on edit mode stuff.
		editModeCollider.enabled = true;

		SetupClickDetector ();
		clickDetector.enabled = true;

		//do necesary things to the taskObject
		if (objInScene == null) {
			objInScene=GameObjectExtensions.GetInterfacesInChildren<ObjInSceneEditPlayInterface> (gameObject);
		}
		foreach (ObjInSceneEditPlayInterface obj in objInScene) {
			obj.OnEnterEditMode ();
		}

	}


	/// <summary>
	/// SimpleClickedOnDetector script calls this function when it detects its being clicked on.
	/// </summary>
	public void ClickedOnMeInEditMode(){
		EventDemux.instance.ReportEventToDemux(MyEventMeanings.ClickedOnItemInScene, gameObject); //launch event
	}

	/// <summary>
	/// Setups the simple clicked on detector
	/// By getting the component (should be on this GO). And assigning its delegado to this so the detector knows who to inform when clicked on.
	/// </summary>
	public void SetupClickDetector(){
		if (!clickDetector) {
			clickDetector = GetComponentInChildren<SimpleClickedOnDetector> ();
			clickDetector.delegado = this;
		}
	}


	/*
	//FOR TESTING
	void Update(){
		if (Input.GetKeyDown (KeyCode.Alpha7)) {
			ToEditMode ();
		}
		if (Input.GetKeyDown (KeyCode.Alpha8)) {
			ToPlayMode ();
		}
	}
*/
		
}
