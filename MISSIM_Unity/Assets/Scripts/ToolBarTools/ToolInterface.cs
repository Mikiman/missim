﻿using UnityEngine;
using System.Collections;

public interface ToolInterface  {



	void SetToolMode(int modeIndex);

	void OnMouseDrag ();

	void Enable (bool enabled);

}
