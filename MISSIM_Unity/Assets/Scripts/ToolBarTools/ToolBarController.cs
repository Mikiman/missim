﻿using UnityEngine;
using System.Collections;

public class ToolBarController : MonoBehaviour {


	public ToolInterface currentTool;
	public Transform SelectedTransform;
	public BoxCollider myCollider;

	public DragTransform3 dragPos;
	public DragRotation dragRot;
	public DragScale dragScale;
	public CameraPositionSelector cameraSelector;


	void Awake(){
		EventDemux.instance.RegisterHandler (this);
	}


	// Use this for initialization
	void Start () {

		if (myCollider == null) {
			myCollider = gameObject.GetComponent<BoxCollider> ();
		}

		//SetTool (1); //Position
		//currentTool.SetToolMode (1);//XYZ


		//Set tool to transform after 10 seconds
		//StartCoroutine(InvokeWithParameterWorkaround(1, 10f));

	}
	/// <summary>
	/// Substitutes Invoke(myFunction,5f); allowing me to pass a parameter
	/// http://answers.unity3d.com/questions/897095/workaround-for-using-invoke-for-methods-with-param.html
	/// </summary>
	/// <param name="toolIndex">Tool index.</param>
	/// <param name="delayTime">Delay time.</param>
	IEnumerator InvokeWithParameterWorkaround(int toolIndex, float delayTime)
	{
		yield return new WaitForSeconds(delayTime);
		// Now do your thing here
		SetTool(toolIndex);
	}



	



	public void SetSelectedTransform(Transform t){
		TurnOnGO ();

		SelectedTransform = t;
		transform.position = t.position;
		transform.rotation = t.rotation;
		transform.localScale = t.localScale;

		//COllider Stuff
		BoxCollider othersColider = t.gameObject.GetComponent<BoxCollider> ();
		myCollider.center = othersColider.center;
		myCollider.size = othersColider.size+ Vector3.one*0.1f;

		//rescale the PparticleSystemRings around selected item 
		dragScale.ResizeParticleSystem(t.localScale);

		//rotate the particle system opposite of this rotation, so selected ring always faces camera.
		//(dragScale.psResizer.ps.transform.localRotation) =Quaternion.Euler(t.eulerAngles*-1f);
		//dragScale.psResizer.ps.transform.localRotation = Quaternion.Inverse(t.rotation); 
	}


	public void SetTool(int index){
	
		if (currentTool != null) {
			currentTool.Enable (false);;
		}


		switch (index) {
		case 0:
			currentTool = (ToolInterface)cameraSelector;
			break;
		case 1:
			currentTool = (ToolInterface)dragPos;
			break;
		case 2:
			currentTool = (ToolInterface)dragRot;
			break;
		case 3:
			currentTool = (ToolInterface)dragScale;
			break;
		
		}

		currentTool.Enable (true);
		Debug.Log ("currentTool: " + currentTool);

	}


	public void SetToolMode(int modeIndex){
		currentTool.SetToolMode (modeIndex);
	}

	/**
	void OnMouseDrag(){
		Debug.Log ("currentTool: " + currentTool);
		currentTool.OnMouseDrag ();
	}
	*/





	public void TurnOffGO(){
		gameObject.SetActive (false);
		transform.position = Vector3.one * (-10000f); //set it far away. this fixes a bug when switching between edit and playmode there is no selected item.
	}
	public void TurnOnGO(){
		gameObject.SetActive (true);
	}




}
