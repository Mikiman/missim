﻿using UnityEngine;
using System.Collections;

public class ParticleSystemResizer : MonoBehaviour {

	public ParticleSystem ps;

	public float modifier= 1f;

	private float _startSize=1f;
	public float startSize{
		get{return _startSize; }
		set{_startSize = value;UpdateStartSize (value); }
	}

	public void UpdateStartSize(float size){

		Debug.Log ("Entered UpdateStartSize(float size)");

		if (ps == null) {
			Debug.LogError ("No particle system in script particleSystemResizer on gameobject"+gameObject.name);
		}else{
			ps.startSize = size * modifier;
		}
	}

	/*
	void Update(){
		UpdateStartSize (_startSize);
	}
	*/
		
}
