﻿using UnityEngine;
using System.Collections;

public class GridCell : MonoBehaviour {

	public Vector3 Position;
	public Transform Element;  //item that can be sitting in the cell;
	private Dimensions dimensions;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public bool IsEmptyCell(){
		return Element == null;
	}
	/**
		Sets the Element held in this cell and if the bool is true, it will parent and resize the transform too.
	*/
	public void SetElement(Transform element,bool parentAndResize){
		Element = element;
		if (parentAndResize) {
			ParentAndResize (element);
		}
	}



	/**
		Makes the given Transform "elem" a child of this transform
		And resizes scale to one
	*/
	public void ParentAndResize(Transform element){
		element.parent = transform;
		element.localScale = Vector3.one;
		element.localPosition = new Vector3 (0, -0.5f, 0);	//fixes a weird bug i dont understand. offset of o,25... like new gameobject cube
	}

	public void SetRendererVisible(bool b){
		GetComponent<MeshRenderer> ().enabled = b;
	}

//	public Vector3 getCenterPosition(){
//		return Position + dimensions.SizeOfCells/2; 
//	}

}
