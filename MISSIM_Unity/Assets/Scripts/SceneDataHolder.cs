﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TFG.Data;
using System;

public class SceneDataHolder : MonoBehaviour {

	public string sceneName;
	//public SceneData sceneData;
	public MetricViewData[] GlobalMetrics;

	public GameObject[] objectsInScene;

	public GameObject[] prefabs;


	public SceneData data;


	// Use this for initialization
	void Start () {
		if (objectsInScene.Length > 0) {
			//Debug.Log ("ObjectInScene PrefabId="+objectsInScene[0].gameObjectInSceneData.PrefabID);


			//SaveAll ("/ObjectsInScene.json");
			//LoadAll ("/ObjectsInScene.json");

		}
		if (prefabs.Length > 0) {
			IdToIndex = new Dictionary<int, int> ();
			FillAuxDictionary ();
		}

	}
	






	public void DestroyGameObject(int indexInScene){
		GameObject gameObj = objectsInScene [indexInScene];
		//If no gameObj,  exit
		if (gameObj == null) {
			return;
		}


		GameObjectInSceneData objdata = gameObj.GetComponent<GameObjectInSceneDataHolder> ().gameObjectInSceneData;
		//If no data, just destroy the object directly and exit
		if (objdata == null) {
			Destroy (gameObj);
			return;
		}


		//TO HANDLE THE JSON DATA SETUP

		int index = objdata.indexInScene;
		//Since array has fixed size, and the order doesnt matter, i replace the item at the given index with the last item in the array, "Deleting" the old values
		objectsInScene[index] = objectsInScene[objectsInScene.Length - 1];


		if (index < data.GOsInScene.Length) {
			Debug.Log ("Deleted an item that was in saved json file");
			data.GOsInScene [index] = data.GOsInScene [data.GOsInScene.Length - 1]; //JSON
			Array.Resize (ref data.GOsInScene, data.GOsInScene.Length - 1); //JSON
		} else {
			Debug.Log ("Deleted an item that wasnt in saved json file");

		}


		//To handle the Gameobject destruction
		Destroy (gameObj);

		// decrement Array's size by one, "deleting" the last element
		Array.Resize(ref objectsInScene, objectsInScene.Length - 1);


		//finally, i update the index of the moved item so its accurate.
		if (index < objectsInScene.Length) {
			objectsInScene [index].GetComponent<GameObjectInSceneDataHolder> ().gameObjectInSceneData.indexInScene = index;
		}


		EventDemux.instance.ReportEventToDemux (MyEventMeanings.ToggleToolBarGO, false);



	}


	/// <summary>
	/// Deletes all objects in scene from the editor.
	/// USE: when going from play to edit, Delete All, then LoadAll();
	/// </summary>
	public void DeleteAll(){
		for (int i = 0; i < objectsInScene.Length; i++) {
			Destroy (objectsInScene[i]);
		}
		data.Clear ();
		Debug.Log ("Delete all");
	}
		

	public void PlayModeToEditMode(string fullPathToLoad){
		DeleteAll ();
		LoadAll (fullPathToLoad);
		//take off edit components on every gameobject in the scene
		foreach (GameObject go in objectsInScene) {
			EditorShell es = go.GetComponent<EditorShell> ();
			if (es) {
				es.ToEditMode ();
			}
		}
	}
	public void EditModeToPlayMode(string fullPathToSave){
		SaveAll (fullPathToSave);

		//take off edit components on every gameobject in the scene
		foreach (GameObject go in objectsInScene) {
			EditorShell es = go.GetComponent<EditorShell> ();
			if (es) {
				es.ToPlayMode ();
			}
		}

		Debug.Log("TODO take off edit components on every gameobject in the scene");
	}


	public void TitleMenuToPlayMode(string fullPathToLoad){
		LoadAll (fullPathToLoad);

		//take off edit components on every gameobject in the scene
		foreach (GameObject go in objectsInScene) {
			Debug.Log ("Entered in go:"+go.name);
			EditorShell es = go.GetComponent<EditorShell> ();
			if (es) {
				es.ToPlayMode ();
				Debug.Log("finished es.toPlayMode()");
			}
		}
		Debug.Log("should have taken off edit components on every gameobject in the scene");
	}
	public void TitleMenuToEditMode(string fullPathToLoad){
		LoadAll (fullPathToLoad);

		//take off edit components on every gameobject in the scene
		foreach (GameObject go in objectsInScene) {
			EditorShell es = go.GetComponent<EditorShell> ();
			if (es) {
				es.ToEditMode ();
			}
		}
	}



	/// <summary>
	/// Creates a JSON string in a JSON FILE with all the data from all the items in the scene.
	/// 
	/// <param name="fullPath"> path+filename+extension, example "/ObjectsInScene.json" </param>
	/// </summary>
	public void SaveAll(string fullPath){


		data.GOsInScene = new GameObjectInSceneData[objectsInScene.Length];
		for(int i=0;i<objectsInScene.Length;i++){
			data.GOsInScene[i] = objectsInScene[i].GetComponent<GameObjectInSceneDataHolder>().gameObjectInSceneData;
		}
			
		//Debug.Log ("Json= " + 	MyComplexFormatter.SerializeToJSON(data); 
		MyComplexFormatter.SaveObjectoToJSONFile(data,fullPath);
		Debug.Log("Saved data to:"+fullPath);


	}

	/// <summary>
	/// Reads jsonstring in a JSON file and fills scene with objects and their data.
	///
	///Steps:
	/// 1) Instantiate the prefab based on data.
	///	2) Pass data and modify transform of new object
	/// 3) Add object to array of objects in scene.	
	/// 
	/// <param name="fullPath"> path+filename+extension, example "/ObjectsInScene.json" </param>
	/// </summary>
	public void LoadAll(string fullPath){

		data = MyComplexFormatter.DeserializeObjectFromJSONFile<SceneData>(fullPath);
		Debug.Log ("Loaded");


		//Create a new Array of objects in scene based on the ones in the saved file:
		objectsInScene = new GameObject[data.GOsInScene.Length];

		for(int i=0;i<data.GOsInScene.Length;i++){
			//Instantiate the prefab based on data.

			//Get its index in the array from its id.
			int prefabIndex = GetPrefabIndexFromId(data.GOsInScene[i].PrefabID);
			GameObject obj= CreatePrefab (prefabIndex);

			//Pass data and modify transform of new object
			obj.GetComponent<GameObjectInSceneDataHolder> ().SetUp (data.GOsInScene [i]);

			//Add object to array of objects in scene.
			objectsInScene [i] = obj;
		}

	}

	public void AddObjectToSceneByPrefabId(int prefabId){
		//Instantiate the prefab based on data.
		//Create the object using its index
		GameObject obj= CreatePrefab (prefabId);

		//Pass data and modify transform of new object
		GameObjectInSceneData data= obj.GetComponent<GameObjectInSceneDataHolder> ().gameObjectInSceneData;
		data.indexInScene= objectsInScene.Length;
		data.position = obj.transform.position;  //unnecesary cause its 0,0,0
		data.rotation = obj.transform.rotation;  //unnecesary cause its 0,0,0,0
		data.scale = obj.transform.localScale;   //necesasry cause prefabs have diferent starting scales.

		//Resize array to fit new one (a list would be better than array, but i want array to see in unity inspector and serializable //TODO change to list
		Array.Resize( ref objectsInScene, objectsInScene.Length+1);

		//Add object to array of objects in scene.
		objectsInScene [objectsInScene.Length-1] = obj;

		//Set up for edit mode.
		EditorShell es = obj.GetComponent<EditorShell> ();
		if (es) {
			es.ToEditMode ();
			Debug.Log ("Calling es.ToEditMode ()");
		}
	
	}


	/// <summary>
	/// Creates the prefab with the given index within the array of possible prefabs
	/// </summary>
	/// <returns>The prefab.</returns>
	/// <param name="prefabIndex">Prefab index.</param>
	public GameObject CreatePrefab(int prefabIndex){

		//create a generic box if the index is out of bounds
		int index = (prefabIndex < prefabs.Length && prefabIndex>=0) ? prefabIndex : 0;


		GameObject item = Instantiate (prefabs [index], Vector3.zero, Quaternion.identity) as GameObject;
		item.transform.SetParent (transform);
		return item;
	}





	//key->Prefab Id,  value--> Prefab index in prefabs array 
	Dictionary<int,int> IdToIndex;

	/// <summary>
	/// Fills the aux dictionary IdToIndex
	/// USE: I store the prefab indexes into the dictionary using key=their id.  that way, i can rearrange the array and objects and i dont have to 
	/// manueally set prefabid=index in array.
	/// </summary>
	public void FillAuxDictionary(){
		for (int i=0;i<prefabs.Length;i++) {
			int key = prefabs [i].GetComponent<GameObjectInSceneDataHolder> ().gameObjectInSceneData.PrefabID;
			IdToIndex.Add (key,i);
			//Debug.Log ("Added key:"+key+", value:"+i);
		}
	}
	/// <summary>
	/// Gets the prefab index from identifier.
	/// </summary>
	/// <returns>The prefab index from identifier.</returns>
	/// <param name="PrefabId">Prefab identifier.</param>
	public int GetPrefabIndexFromId(int PrefabId){
		if (IdToIndex==null) {
			IdToIndex = new Dictionary<int, int> ();
			FillAuxDictionary();
		}

		int index=IdToIndex [PrefabId]; 		
		Debug.Log ("Looking for key:"+PrefabId+" and value="+index);
		return index;
	}

}
