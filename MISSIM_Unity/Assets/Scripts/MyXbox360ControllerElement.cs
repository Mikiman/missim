﻿using UnityEngine;
using System.Collections;

public class MyXbox360ControllerElement : MonoBehaviour {

	// See: https://www.youtube.com/watch?v=384_g0f7K_I


	public bool isButton;
	//public string buttonName="AButton";
	public BUTTONS button = BUTTONS.A;
	public bool leftJoystick,rightJoystick,isDPad,LTrigger,RTrigger;


	private Vector3 StartPos;
	private Transform ThisTransform;
	private MeshRenderer mr;	
	public string Horizontal="", Vertical="";
	public float Debug_Horizontal_float, Debug_Vertical_float;


	private string[] ButtonNames = {"AButton","BButton","XButton","YButton","LBumperButton","RBumperButton","BackButton","StartButton","LThumbStickClickButton","RThumbstickClickButton"};
	public enum BUTTONS {A=0,B=1,X=2,Y=3,LBumper=4,RBumper=5,Back=6,Start=7,LThumbStickClick=8,RThumbstickClick=9};



	// Use this for initialization
	void Start () {
		ThisTransform = transform;
		StartPos = ThisTransform.localPosition;
		mr = ThisTransform.GetComponent<MeshRenderer> ();
		SetHorizontalAndVertical ();
	}

	// Update is called once per frame
	void Update () {
		if (isButton) {
			//ALL THREE WAYS WORK
			//mr.enabled = Input.GetButton (buttonName);
			mr.enabled = Input.GetButton (ButtonNames[(int)button]);
			//mr.enabled = Input.GetKey(KeyCode.JoystickButton5);
		} else {
			/*
			if(leftJoystick){
				Vector3 inputDirection = Vector3.zero;
				inputDirection.x = Input.GetAxis ("LeftJoystickHorizontal");
				inputDirection.z = Input.GetAxis ("LeftJoystickVertical");
				ThisTransform.position = StartPos + inputDirection;
			}
			else if(rightJoystick){
				Vector3 inputDirection = Vector3.zero;
				inputDirection.x = Input.GetAxis ("RightJoystickHorizontal");
				inputDirection.z = Input.GetAxis ("RightJoystickVertical");
				ThisTransform.position = StartPos + inputDirection;
			}
			*/


			//StartPos = transform.parent.position;
		

			Vector3 inputDirection = Vector3.zero;
			//only move if the input exits.
			if(!string.Equals(Horizontal,"")){inputDirection.x=Debug_Horizontal_float = Input.GetAxis (Horizontal);}
			if (!string.Equals(Vertical,"")) {inputDirection.z=Debug_Vertical_float = Input.GetAxis (Vertical);}
			ThisTransform.position = transform.parent.position + StartPos + inputDirection;
				
		}
	}


	private void  SetHorizontalAndVertical(){
		if (leftJoystick) {
			Horizontal = "LeftJoystickHorizontal";
			Vertical = "LeftJoystickVertical";
		} else if (rightJoystick) {
			Horizontal = "RightJoystickHorizontal";
			Vertical = "RightJoystickVertical";
		} else if (isDPad) {
			Horizontal = "DPadHorizontal";
			Vertical = "DPadVertical";
		} else if (LTrigger) {
			//Horizontal = "LeftTrigger";
			Vertical = "LeftTrigger";
		} else if (RTrigger) {
			//Horizontal = "RightTrigger";
			Vertical = "RightTrigger";
		} else {
			Horizontal = "";
			Vertical = "";
			return; //si no es nada, no sigas.
		}
	}
}
