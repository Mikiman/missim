﻿using UnityEngine;
using System.Collections;

public class CameraPositionSelector : MonoBehaviour,ToolInterface {

	/*
	public Transform[] cams;
	public Transform active;

	public int cameraIndex=0;

	public KeyCode CameraToggleKey = KeyCode.Alpha9;


	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (CameraToggleKey)) {

			cameraIndex = (1+cameraIndex) % cams.Length;

			active.position = cams[cameraIndex].position;
			active.rotation = cams[cameraIndex].rotation;


		}
	}

	*/


	public Camera[] cams;
	//public Camera active;

	public int cameraIndex=0;

	public KeyCode CameraToggleKey = KeyCode.Alpha9;


	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (CameraToggleKey)) {
			toNextCamera ();
		}
	}


	public void toNextCamera(){
		//turn off current camera
		cams [cameraIndex].gameObject.SetActive (false);

		//Increment and loop index
		cameraIndex = (1+cameraIndex) % cams.Length;

		//turn on next camera
		cams [cameraIndex].gameObject.SetActive (true);

	}



	/*
		ToolInterface stuff
	*/
	public void Enable(bool enabled){
		this.enabled = enabled;
	}
	public void SetToolMode(int modeIndex){
		//turn off current camera
		cams [cameraIndex].gameObject.SetActive (false);

		//Get next index with loop
		cameraIndex = (modeIndex) % cams.Length;

		//turn on next camera
		cams [cameraIndex].gameObject.SetActive (true);

	}
	public void OnMouseDrag (){
		if (!enabled) {
			return;
		}
	}




}
