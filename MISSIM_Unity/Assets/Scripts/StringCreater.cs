﻿using UnityEngine;
using System.Collections;

public class StringCreater : MonoBehaviour {
	public GameObject StringSegmentPrefab,StringJointPrefab;
	public Transform CreateAtPosition;
	public int NumberOfSegments=1;
	// Use this for initialization
	void Start () {
		for (int i = 0; i < NumberOfSegments; i++) {
			CreateJoint ();
			CreateStringSegment ();

		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void CreateStringSegment(){
		GameObject segment = Instantiate (StringSegmentPrefab,CreateAtPosition.position,Quaternion.identity) as GameObject;
		segment.transform.SetParent(CreateAtPosition.parent);
		CreateAtPosition = segment.transform.GetChild (0);
		HingeJoint hj = segment.GetComponent<HingeJoint> ();
		hj.connectedBody = segment.transform.parent.GetComponent<Rigidbody> ();
	}

	public void CreateJoint(){
		GameObject joint = Instantiate (StringJointPrefab,CreateAtPosition.position,Quaternion.identity) as GameObject;
		joint.transform.SetParent(CreateAtPosition.parent);
		CreateAtPosition = joint.transform.GetChild (0);
		HingeJoint hj = joint.GetComponent<HingeJoint> ();
		hj.connectedBody = joint.transform.parent.GetComponent<Rigidbody> ();
	}

}
