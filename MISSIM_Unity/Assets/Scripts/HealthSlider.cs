﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthSlider : MonoBehaviour {
	public Slider slider;

	
	// Update is called once per frame
	void Update () {
		slider.value = Mathf.MoveTowards (slider.value, 100.0f, 0.15f);
	}
}
