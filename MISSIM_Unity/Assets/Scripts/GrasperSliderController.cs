﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class GrasperSliderController : MonoBehaviour {


	public Rigidbody gripperRB;

	//public Transform TargetTransform;

	//public Slider PosXSlider;
	//public Slider PosYSlider;
	//public Slider PosZSlider;

	//public Slider RotXSlider;
	//public Slider RotYSlider;
	public Slider RotZSlider;
	public bool ValueChanged;
	public float MoveForce;


	[SerializeField]
	private Vector3 DefaultPosition;
	[SerializeField]
	private Vector3 DefaultRotation;
	//[SerializeField]
	//private float DefaultZoom;

	private Quaternion deltaRotation= new Quaternion();




	// Use this for initialization
	void Start () {
		gripperRB= transform.GetChild(0).GetComponent<Rigidbody> ();

		if (gripperRB == null) {
			return;
		}
		SetDefaultValues ();

	}

	// Update is called once per frame
	void Update () {

	}

	public void SetDefaultValues(){
		DefaultPosition = gripperRB.transform.position;
		DefaultRotation = gripperRB.transform.eulerAngles;
		DefaultRotation = RotationBetween180 (DefaultRotation);//These ifs fix a bug caused by maxValue = 180 and start =270 (=-90)
		setUpSliders ();
	}

	public void SliderValueChanged(){
		//TargetTransform.position = new Vector3 (PosXSlider.value,PosYSlider.value,PosZSlider.value);
		//TargetTransform.rotation = Quaternion.Euler (RotXSlider.value,RotYSlider.value,RotZSlider.value);
		//gripperRB.position = new Vector3 (PosXSlider.value,PosYSlider.value,PosZSlider.value);

		//gripperRB.isKinematic = false;
		//gripperRB.MoveRotation(Quaternion.Euler (0f,0f,gripperRB.rotation.z + RotZSlider.value));
		//gripperRB.isKinematic = true;
		ValueChanged = true;

	}

	void FixedUpdate(){
		//gripperRB.isKinematic = false;




/*

		if (ValueChanged) {
			deltaRotation = Quaternion.Euler (new Vector3(0f,0f,RotZSlider.value)*Time.deltaTime);
			gripperRB.MoveRotation(gripperRB.rotation*deltaRotation);
		}

		if (gripperRB.rotation.z> 180) {
			gripperRB.rotation =Quaternion.Euler(new Vector3(gripperRB.rotation.x,gripperRB.rotation.y,gripperRB.rotation.z-360));
		}
		if (Mathf.Abs (gripperRB.rotation.z) < RotZSlider.minValue+0.2) {	//the +0.2 helps it get unstuck from a precision based epsilon bug.
			ValueChanged = false;
		}


		*/


		Vector3 Force = new Vector3 (0f, 0f, Input.GetAxisRaw ("Horizontal")) * MoveForce;
		gripperRB.AddForce (Force, ForceMode.Acceleration);


		//Quaternion Rotation = Quaternion.Euler(new Vector3 (0f, 0f, 5) * MoveForce*Time.deltaTime);
		//gripperRB.MoveRotation (gripperRB.rotation*Rotation);


		Quaternion deltaRotation = Quaternion.Euler(new Vector3 (0f, 0f, 5) * Time.deltaTime);
		gripperRB.MoveRotation(gripperRB.rotation * deltaRotation);




		//gripperRB.isKinematic = true;
	}


	private Vector3 RotationBetween180(Vector3 rotation){
		//These ifs fix a bug caused by maxValue = 180 and start =270 (=-90)
		//if (rotation.x > RotXSlider.maxValue) {
		//	rotation.x -= 360;
		//}
		//if (rotation.y > RotYSlider.maxValue) {
		//	rotation.y -= 360;
		//}
		if (rotation.z > RotZSlider.maxValue) {
			rotation.z -= 360;
		}
		return rotation;
	}
	private void setUpSliders(){
		Vector3 Pos = gripperRB.transform.position;
		Vector3 Rot = gripperRB.transform.eulerAngles;

		//PosXSlider.value = Pos.x;
		//PosYSlider.value = Pos.y;
		//PosZSlider.value = Pos.z;

		Rot = RotationBetween180 (Rot);
		//RotXSlider.value = Rot.x;
		//RotYSlider.value = Rot.y;
		RotZSlider.value = Rot.z;

	}

	public void ResetTransformToDefault(){
		//TargetTransform.position = DefaultPosition;
		//TargetTransform.eulerAngles = DefaultRotation;

		setUpSliders ();
	}

	public void SetTarget(Transform targ){
		//TargetTransform = targ;
		SetDefaultValues ();
	}
}
