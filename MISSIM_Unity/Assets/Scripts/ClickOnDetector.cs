﻿using UnityEngine;
using System.Collections;

public class ClickOnDetector : MonoBehaviour {

	//http://answers.unity3d.com/questions/373818/how-to-detect-mouse-click-on-a-gameobject.html

	public bool clickedOn = false;

	public void OnMouseOver(){
		if(Input.GetMouseButton(0)){
			clickedOn = true;
		}
	}

}
