var gizmoAxis: GameObject;
var gizmoSize: float = 1.0;

private var gizmoObj: GameObject;
private var gizmo: Gizmo;
private var gizmoType: GizmoMyType = GizmoMyType.Position;

function Update () {
    if (Input.GetKeyDown(KeyCode.Escape)) {
        removeGizmo();
    }
    
    if (gizmo) {
        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            gizmoType = GizmoMyType.Position;
            gizmo.setType(gizmoType);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2)) {
            gizmoType = GizmoMyType.Rotation;
            gizmo.setType(gizmoType);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3)) {
            gizmoType = GizmoMyType.Scale;
            gizmo.setType(gizmoType);
        }        
        if (gizmo.needUpdate) {
            resetGizmo();
        }
    }


}


function OnMouseDown() {	
	//Debug.Log("entered in OnMouseDown Gizmo");
    if (!gizmoObj) {
        resetGizmo();
    }
}

function removeGizmo() {
    if (gizmoObj) {
        gameObject.layer = 0;
        for (var child : Transform in transform) {
            child.gameObject.layer = 0;
        }        
        Destroy(gizmoObj);    
        Destroy(gizmo);    
    }
}

function resetGizmo() {
	//Debug.Log("entered in reset Gizmo");
    removeGizmo();
    gameObject.layer = 2;
    for (var child : Transform in transform) {
        child.gameObject.layer = 2;
    }        
    gizmoObj = Instantiate(gizmoAxis, transform.position, transform.rotation);
    gizmoObj.transform.localScale *= gizmoSize;
    gizmo = gizmoObj.GetComponent("Gizmo");
    gizmo.setParent(transform);
    gizmo.setType(gizmoType);
}

