var axisX: GizmoHandle;
var axisY: GizmoHandle;
var axisZ: GizmoHandle;

var gizmoType: GizmoMyType;

var needUpdate: boolean = false;

function Awake() {
    axisX.axis = GizmoAxis.X;
    axisY.axis = GizmoAxis.Y;
    axisZ.axis = GizmoAxis.Z;
    
    setType(gizmoType);
}


function Update () {    
    needUpdate = (axisX.needUpdate || axisY.needUpdate || axisZ.needUpdate);
}

function setType(type: GizmoMyType) {
    axisX.setType(type);
    axisY.setType(type);
    axisZ.setType(type);
}

function setParent(other: Transform) {
    transform.parent = other;
    axisX.setParent(other);
    axisY.setParent(other);
    axisZ.setParent(other);
}
