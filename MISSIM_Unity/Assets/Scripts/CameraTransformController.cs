﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CameraTransformController : MonoBehaviour {
	
	public Slider PosXSlider;
	public Slider PosYSlider;
	public Slider PosZSlider;

	public Slider RotXSlider;
	public Slider RotYSlider;
	public Slider RotZSlider;

	public Slider CameraZoomSlider;

	[SerializeField]
	private Vector3 DefaultPosition;
	[SerializeField]
	private Vector3 DefaultRotation;
	[SerializeField]
	private float DefaultZoom;




	private Camera cam;
	// Use this for initialization
	void Start () {
		cam = Camera.main;
		DefaultPosition = cam.transform.position;
		DefaultRotation = cam.transform.eulerAngles;
		DefaultZoom = cam.fieldOfView;


	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SliderValueChanged(){
		cam.transform.position = new Vector3 (PosXSlider.value,PosYSlider.value,PosZSlider.value);
		cam.transform.rotation = Quaternion.Euler (RotXSlider.value,RotYSlider.value,RotZSlider.value);
		cam.fieldOfView = CameraZoomSlider.value;
	
	}

	private void setUpSliders(){
		Vector3 Pos = cam.transform.position;
		Vector3 Rot = cam.transform.eulerAngles;

		CameraZoomSlider.value = cam.fieldOfView;

		PosXSlider.value = Pos.x;
		PosYSlider.value = Pos.y;
		PosZSlider.value = Pos.z;

		RotXSlider.value = Rot.x;
		RotYSlider.value = Rot.y;
		RotZSlider.value = Rot.z;



	}

	public void ResetCameraToDefault(){
		cam.transform.position = DefaultPosition;
		cam.transform.eulerAngles = DefaultRotation;
		cam.fieldOfView = DefaultZoom;

		setUpSliders ();
	}


}
