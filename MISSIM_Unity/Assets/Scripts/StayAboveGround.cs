﻿using UnityEngine;
using System.Collections;

public class StayAboveGround : MonoBehaviour {

	public float lowestHeight=0;
	Vector3 position;
	// Update is called once per frame
	void LateUpdate () {
		position = transform.position;
		if (position.y <= lowestHeight) {
			transform.position = new Vector3 (position.x, lowestHeight, position.z);
		}
	}
}
