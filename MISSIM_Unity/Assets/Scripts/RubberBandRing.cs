﻿using UnityEngine;
using System.Collections;

public class RubberBandRing : MonoBehaviour {

	private Quaternion q;
	private Vector3 v3;
	private bool hasHit = false;
	Rigidbody MyRigidbody;
	public bool StickInWallOrFallDown=false; //true: stick,  false: fall

	// Use this for initialization
	void Start () {            
		MyRigidbody = this.GetComponent<Rigidbody>();   
	}

	void OnCollisionEnter(Collision col)
	{   
		MyRigidbody.isKinematic = true;    
		hasHit = true;
	}

	void LateUpdate() {
		if (hasHit) {
			transform.position = v3;
			transform.rotation = q;
			hasHit = false;

			//if FallDown, do this
			if (!StickInWallOrFallDown) {
				MyRigidbody.isKinematic = false;
			}
		}
		else {
			v3 = transform.position;
			q = transform.rotation;
		}
	}
}


