﻿using UnityEngine;
using System.Collections;

//https://aarlangdi.blogspot.com.es/2013/11/drag-and-drop-in-unity.html

public class DragTransform3: MonoBehaviour,ToolInterface {

	public bool enabled = true;


	private Vector3 screenPoint;

	public Vector3 offset;
	public float dragSpeed=1f;


	public enum MovementPlane
	{
		xyz,xz,xy,yz,z,x,y
	}
	public MovementPlane plane=MovementPlane.xyz;

	void OnMouseDown(){
		screenPoint = Camera.main.WorldToScreenPoint (transform.position); 
	}



	public void Enable(bool enabled){
		this.enabled = enabled;
	}


	public void OnMouseDrag()
	{

		if (!enabled) {
			return;
		}

		Vector3 mousePos = Input.mousePosition;
		Vector3 currentScrenPoint;// = new Vector3 (mousePos.x, mousePos.y, screenPoint.z);
		Vector3 currentPos=new Vector3();// = Camera.main.ScreenToWorldPoint (currentScrenPoint);

		switch (plane) {
		case MovementPlane.xz:
			//FORMA 1
			//currentScrenPoint = new Vector3 (mousePos.x, 0f, mousePos.y);
			//currentPos = Camera.main.ScreenToWorldPoint (currentScrenPoint);

			//FORMA 2
			currentPos = transform.position;
			currentPos.x += (Input.GetAxis("Mouse X") * dragSpeed); //adds the mouse delta (times a speed factor) to the position
			currentPos.z += (Input.GetAxis("Mouse Y") * dragSpeed);

			currentPos.y = transform.position.y;
			break;
		case MovementPlane.xy:
			currentScrenPoint = new Vector3 (mousePos.x, mousePos.y, 0f);
			currentPos = Camera.main.ScreenToWorldPoint (currentScrenPoint);

			currentPos.z = transform.position.z;
			break;
		case MovementPlane.yz:
			currentScrenPoint = new Vector3 (0f,mousePos.x , screenPoint.y);
			currentPos = Camera.main.ScreenToWorldPoint (currentScrenPoint);

			currentPos.x = transform.position.x;
			break;
		case MovementPlane.xyz:
			currentScrenPoint = new Vector3 (mousePos.x, mousePos.y, screenPoint.z);
			currentPos = Camera.main.ScreenToWorldPoint (currentScrenPoint);
			break;
		
		case MovementPlane.x:
			currentScrenPoint = new Vector3 (mousePos.x, mousePos.y, screenPoint.z);
			currentPos = Camera.main.ScreenToWorldPoint (currentScrenPoint);

			currentPos.y = transform.position.y;
			currentPos.z = transform.position.z;
			break;

		case MovementPlane.y:
			currentScrenPoint = new Vector3 (mousePos.x, mousePos.y, screenPoint.z);
			currentPos = Camera.main.ScreenToWorldPoint (currentScrenPoint);

			currentPos.x = transform.position.x;
			currentPos.z = transform.position.z;
			break;

		case MovementPlane.z:
			currentScrenPoint = new Vector3 (mousePos.x, mousePos.y, screenPoint.z);
			currentPos = Camera.main.ScreenToWorldPoint (currentScrenPoint);

			currentPos.x = transform.position.x;
			currentPos.y = transform.position.y;
			break;
		}


		//Clamps movements to scenario entorno
		currentPos.x= Mathf.Clamp (currentPos.x, -45, 45);
		currentPos.y= Mathf.Clamp (currentPos.y, 0, 35);
		currentPos.z= Mathf.Clamp (currentPos.z, 0, 90);


		transform.position = currentPos;

		EventDemux.instance.ReportEventToDemux (MyEventMeanings.SetTransform, new TFG.Data.MyTransformData {
			position = currentPos,
			setPos = true
		});
	}





	public void SetToolMode(int index){
		switch (index) {
		case 1:
			plane = MovementPlane.xyz;
			break;

		case 2:
			plane = MovementPlane.xz;
			break;
		case 3:
			plane = MovementPlane.xy;
			break;
		case 4:
			plane = MovementPlane.yz;
			break;
		case 5:
			plane = MovementPlane.z;
			break;
		case 6:
			plane = MovementPlane.x;
			break;
		case 7:
			plane = MovementPlane.y;
			break;
		}
	}



}
