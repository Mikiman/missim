﻿using UnityEngine;
using System.Collections;

//https://aarlangdi.blogspot.com.es/2013/11/drag-and-drop-in-unity.html

public class DragTransform2 : MonoBehaviour {

	private Vector3 screenPoint;



	void OnMouseDown(){
		screenPoint = Camera.main.WorldToScreenPoint (transform.position); 
	}

	void OnMouseDrag()
	{
		Vector3 mousePos = Input.mousePosition;
		Vector3 currentScrenPoint = new Vector3 (mousePos.x, mousePos.y, screenPoint.z);
		Vector3 currentPos = Camera.main.ScreenToWorldPoint (currentScrenPoint);
		transform.position = currentPos;
	}
}
