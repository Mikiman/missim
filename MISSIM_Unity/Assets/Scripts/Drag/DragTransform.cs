﻿
using System.Collections;
using UnityEngine;

//Tobias J. : https://forum.unity3d.com/threads/implement-a-drag-and-drop-script-with-c.130515/

class DragTransform : MonoBehaviour
{
	public Color mouseOverColor = Color.blue;
	public Color originalColor = Color.yellow;
	private bool dragging = false;
	private float distance;

	private Renderer renderer;

	void Start(){
		renderer = GetComponent<Renderer> ();
	}

	void OnMouseEnter()
	{
		renderer.material.color = mouseOverColor;
	}

	void OnMouseExit()
	{
		renderer.material.color = originalColor;
	}

	void OnMouseDown()
	{
		distance = Vector3.Distance(transform.position, Camera.main.transform.position);
		dragging = true;
	}

	void OnMouseUp()
	{
		dragging = false;
	}

	void Update()
	{
		if (dragging)
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			Vector3 rayPoint = ray.GetPoint(distance);
			transform.position = rayPoint;
		}
	}
}
