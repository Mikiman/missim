﻿using UnityEngine;
using System.Collections;

public class DragRotation : MonoBehaviour, ToolInterface{


	public float speed=200;
	public bool enabled=true;
	public bool allowOnYAxis = true;
	public bool allowOnXAxis = true;
	public bool allowOnZAxis = true;


	public void Enable(bool enabled){
		this.enabled = enabled;
	}

	public void SetToolMode(int index){
		switch (index) {
		case 1:
			allowOnXAxis = true;
			allowOnYAxis = true;
			allowOnZAxis = true;
			break;

		case 2:
			allowOnXAxis = false;
			allowOnYAxis = true;
			allowOnZAxis = false;
			break;
		case 3:
			allowOnXAxis = false;
			allowOnYAxis = false;
			allowOnZAxis = true;
			break;
		case 4:
			allowOnXAxis = true;
			allowOnYAxis = false;
			allowOnZAxis = false;
			break;
		}
	}



	public void OnMouseDrag(){
		if (!enabled) {
			return;
		}

		/*
		 * //GOOD CODE  https://www.youtube.com/watch?v=S3pjBQObC90
		float rotX = Input.GetAxis ("Mouse X") * speed * Mathf.Deg2Rad;
		float rotY = Input.GetAxis ("Mouse Y") * speed * Mathf.Deg2Rad;

		transform.RotateAround (Vector3.up, -rotX);
		transform.RotateAround (Vector3.right, rotY);
		*/

		//BETTER CODE
		//  http://answers.unity3d.com/questions/208864/spinning-an-object-with-mouse-movement.html
		//transform.Rotate (Input.GetAxis ("Mouse Y") * speed * Mathf.Deg2Rad, -Input.GetAxis ("Mouse X") * speed * Mathf.Deg2Rad, 0, Space.World);
		float x=0f,y=0f,z= 0f;
		if (allowOnXAxis) {
			x = Input.GetAxis ("Mouse Y") * speed * Mathf.Deg2Rad;
		}
		if (allowOnYAxis) {
			y = -Input.GetAxis ("Mouse X") * speed * Mathf.Deg2Rad;
		}
		if (allowOnZAxis) {
			z = -Input.GetAxis ("Mouse X") * speed * Mathf.Deg2Rad;
		}

		transform.Rotate (x, y, z, Space.World);

		EventDemux.instance.ReportEventToDemux (MyEventMeanings.SetTransform, new TFG.Data.MyTransformData {
			rotation = transform.rotation,
			setRot = true
		});
	}


	//SEE FOR SWIPING: https://www.youtube.com/watch?v=zW1lxrgHgG8
}
