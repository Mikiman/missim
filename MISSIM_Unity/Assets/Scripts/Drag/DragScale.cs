﻿using UnityEngine;
using System.Collections;

public class DragScale : MonoBehaviour, ToolInterface{


	public float speed=10;
	public bool enabled=false;
	public bool allowOnYAxis = true;
	public bool allowOnXAxis = true;
	public bool allowOnZAxis = true;

	public ParticleSystemResizer psResizer;


	public void Enable(bool enabled){
		this.enabled = enabled;
	}

	public void SetToolMode(int index){
		switch (index) {
		case 1:
			allowOnXAxis = true;
			allowOnYAxis = true;
			allowOnZAxis = true;
			break;

		case 2:
			allowOnXAxis = true;
			allowOnYAxis = false;
			allowOnZAxis = false;
			break;
		case 3:
			allowOnXAxis = false;
			allowOnYAxis = true;
			allowOnZAxis = false;
			break;
		case 4:
			allowOnXAxis = false;
			allowOnYAxis = false;
			allowOnZAxis = true;
			break;
		}
	}



	public void OnMouseDrag(){
		if (!enabled) {
			return;
		}


		float x=0f,y=0f,z= 0f;

		if(allowOnXAxis&&allowOnYAxis&&allowOnZAxis){
			x=Input.GetAxis ("Mouse Y") * speed;
			y = x;
			z = x;
		}else{
			if (allowOnXAxis) {
				x = Input.GetAxis ("Mouse X") * speed;
			}
			if (allowOnYAxis) {
				y = Input.GetAxis ("Mouse Y") * speed;
			}
			if (allowOnZAxis) {
				z = Input.GetAxis ("Mouse Y") * speed;
			}
		}
			
		transform.localScale += new Vector3 (x, y, z);

		EventDemux.instance.ReportEventToDemux (MyEventMeanings.SetTransform, new TFG.Data.MyTransformData {
			scale = transform.localScale,
			setScale = true
		});

		ResizeParticleSystem (transform.localScale);
	}


	public void ResizeParticleSystem(Vector3 localScale){
		//To resize the start size of the particle.
		//psResizer.startSize = Mathf.Abs(Mathf.Max (localScale.x, localScale.y, localScale.z));

		//TO resize the radius the particles are emmited arround.
		var x = psResizer.ps.shape;
		x.radius = Mathf.Abs(Mathf.Max (localScale.x, localScale.y, localScale.z));
	}


	//SEE FOR SWIPING: https://www.youtube.com/watch?v=zW1lxrgHgG8
}
