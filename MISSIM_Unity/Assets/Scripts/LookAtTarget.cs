﻿using UnityEngine;
using System.Collections;

public class LookAtTarget : MonoBehaviour {

	public Transform TargetPosition;
	public float Speed;
	private Rigidbody rb;

	public Vector3 Forward,v,CurrentRotation,DestinationRotation;


	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate(){
		//transform.LookAt (TargetPosition.position);

		CurrentRotation = transform.rotation.eulerAngles;
		DestinationRotation = TargetPosition.position - transform.position;

		if (DestinationRotation == Vector3.zero){return;}

		Quaternion deltaRotation = Quaternion.Euler(DestinationRotation * Time.deltaTime*Speed);
		rb.MoveRotation(rb.rotation * deltaRotation);

		//rb.AddTorque (DestinationRotation - CurrentRotation);



		/*
		v = (TargetPosition.position - transform.position).normalized;
		Forward = transform.forward.normalized;
		if(v==transform.forward.normalized){v = Vector3.zero;return;}

		Quaternion deltaRotation = Quaternion.Euler(v * Time.deltaTime*Speed);
		rb.MoveRotation(rb.rotation * deltaRotation);
		*/


	}
}
