﻿using UnityEngine;
using System.Collections;

public class MyInputHandler : MonoBehaviour {

	public Rigidbody rb_RightInsertion,rb_LeftInsertion, rb_RightGrasperTop,rb_RightGrasperBottom,rb_LeftGrasperTop,rb_LeftGrasperBottom;
	public Transform RightAimTarget,LeftAimTarget;
	public bool AimTargetsVisible=true;
	public Vector3 inputDirection;


	private Vector3 StartPos;
	public MovementParams Mov_Params;
	public Vector3 debugFloats;
	private JointMotor RightTopGrasperMotor, RightBottomGrasperMotor,LeftTopGrasperMotor, LeftBottomGrasperMotor;
	private HingeJoint RightTopGrasperHinge, RightBottomGrasperHinge,LeftTopGrasperHinge, LeftBottomGrasperHinge;

	//Used for Grasper Pinching Control
	private bool[] KeepRotatingOnThisAxis = { true, true, true };
	public const int Yaw = 2,Torsion = 1,Ptich =0;


	public bool updateTorsion, updateAim, updateInsertion,updatePinch;

	[System.Serializable]
	public class MovementParams{
		[Tooltip("Moving speed of the grasper target transform")]
		public float GrasperAimSpeed=1f; 

		[Tooltip("Rotating speed of the grasper rigid body")]
		public float TorsionSpeed=1f;

		[Tooltip("Pinching speed of the grasper rigid body")]
		public Vector3 GrapserPinchSpeed   = new Vector3 (0,0,30f);
		public Vector3 MaxPincherRotations = new Vector3 (0,0,30f);

		[Tooltip("Insertion speed of the Pole rigid body")]
		public float InsertionSpeed=1f;


		[Tooltip("Clamps the position of the moving target between theese boundries, When using EVA, make this false")]
		public bool ClampAimPointsWithinBounds=true;
		public Vector3 MinPos = Vector3.zero;
		public Vector3 MaxPos = Vector3.one;

		[Tooltip("If joystick value is less then this, count it as 0")]
		public Vector3 MinAxisValues = Vector3.zero;



	}







	// Use this for initialization
	void Start () {
		if (RightAimTarget == null) {
			Debug.Log ("No RightAimTarget Transform in MyInputHandler Script on GameObject:"+this.name);
		}
		StartPos = RightAimTarget.transform.position;

		//SetUpHingeJointsInGrasper ();
		SetUpHingeJointsInGrasper2Hands();


		//rb_GrasperTop1 = transform.GetChild(0).GetComponent<Rigidbody>();
		//rb_GrasperBottom = transform.GetChild(1).GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame

	void Update () {
		if (updateAim) {
			UpdateAimTargetPosition (true); //right hand
			UpdateAimTargetPosition (false); //left hand
		}


		UpdateAimTargetVisibility();
		//UpdateControlLayout();
	

	}



	void FixedUpdate(){
		if (updatePinch) {
			UpdateGrasperPinch ();
		}
		if (updateTorsion) {
			UpdateInsertionTorsion ();
		}
		if (updateInsertion) {
			UpdateInsertionProfundidad ();
		}
	}




	/**
		Pasos:
			1: Get input from joystick;
			2: Clamps input to zero if below the threashold set in Mov_Params.MinAxisValues;
			3: Moves the transform of the Grasper Target
			4: Clamps transform of grasper target to stay in boundries of entorno set in MovParams_MinPos and MaxPos.
			5: thanks to the look at target controller on the Insertion rotator, the rotator looks at the given target through physics in that script.
	*/
	private void UpdateRightPiezaManoAimTarget(){
		inputDirection = Vector3.zero;
		inputDirection.x = Input.GetAxis ("RightJoystickHorizontal");
		inputDirection.z = Input.GetAxis ("RightJoystickVertical");

		//Floors input to 0 if below a threshhold.
		for (int i = 0; i < 3; i++) {
			if (Mathf.Abs(inputDirection [i]) < Mov_Params.MinAxisValues [i]) {
				inputDirection [i] = 0;
			}
		}
			
		//RightGrasperTarget.position = StartPos + inputDirection * Speed;   //this goes back to (0,0,0,)
		RightAimTarget.position += inputDirection * Mov_Params.GrasperAimSpeed;	//this adds ++

		Vector3 ClampedPos = Vector3.zero;
		//Clamp the target within boundries of Entorno.
		for (int i = 0; i < 3; i++) {
			ClampedPos[i] = Mathf.Clamp (RightAimTarget.position [i],Mov_Params.MinPos[i],Mov_Params.MaxPos[i]);
		}
		RightAimTarget.position = ClampedPos;
	}



	private void UpdateAimTargetPosition(bool RightHand){

		inputDirection = Vector3.zero;
		Transform Target =RightAimTarget;  //lo defino al principio para arreglar un bug. y no se como hacer new Transform();


			//RIGHT HAND
		if (RightHand) {
				inputDirection.x = -Input.GetAxis ("RightJoystickHorizontal"); 	//Signo menos por inversion de palanca
				inputDirection.z = -Input.GetAxis ("RightJoystickVertical");
				Target = RightAimTarget;
			}
		else{//Left HAND
				inputDirection.x = -Input.GetAxis ("LeftJoystickHorizontal");
				inputDirection.z = -Input.GetAxis ("LeftJoystickVertical");
				Target = LeftAimTarget;
		}




		//Floors input to 0 if below a threshhold.
		for (int i = 0; i < 3; i++) {
			if (Mathf.Abs(inputDirection [i]) < Mov_Params.MinAxisValues [i]) {
				inputDirection [i] = 0;
			}
		}

		//RightGrasperTarget.position = StartPos + inputDirection * Speed;   //this goes back to (0,0,0,)
		Target.position += inputDirection * Mov_Params.GrasperAimSpeed;	//this adds ++


		//When using EVA, make this false
		if(Mov_Params.ClampAimPointsWithinBounds){
			//WORKING GOOD CODE to clamp the aim within bounds
			Vector3 ClampedPos = Vector3.zero;
			//Clamp the target within boundries of Entorno.
			for (int i = 0; i < 3; i++) {
				ClampedPos[i] = Mathf.Clamp (Target.position [i],Mov_Params.MinPos[i],Mov_Params.MaxPos[i]);
			}
			
			Target.position = ClampedPos;
		}


	
	}

	private void UpdateAimTargetVisibility(){
		if (Input.GetButtonDown ("BackButton")) {
			AimTargetsVisible = !AimTargetsVisible;
			RightAimTarget.GetComponent<MeshRenderer> ().enabled = AimTargetsVisible;
			LeftAimTarget.GetComponent<MeshRenderer> ().enabled = AimTargetsVisible;
		}
	}




	/******************************************************************************************************************************************
	 * 			Grasper Pinch Methods:
	 * *****************************************************************************************************************************************/

	void UpdateGrasperPinch(){
		//ClampAtMax ();
		//ClampAtMax2();
		//SimpleRotate ();
		UpdateGrasperPinchMotors();
	}

	private void SimpleRotate(){

		Vector3 Velocities = Mov_Params.GrapserPinchSpeed;

		for (int i = 0; i < 3; i++) {
			if (!KeepRotatingOnThisAxis [i]) {
				Velocities [i] = 0;
				KeepRotatingOnThisAxis [i] = true;	//reset values for next framestep
			}
		}

		//Totates top part of Grasper down to zero
		Velocities.z = Mov_Params.GrapserPinchSpeed.z * -Input.GetAxisRaw ("RightTrigger");
		//Velocities.z = Mov_Params.GrapserPinchSpeed.z * -Input.GetAxisRaw ("RightTrigger");
		Quaternion deltaRotation = Quaternion.Euler(Velocities * Time.deltaTime);
		rb_RightGrasperTop.MoveRotation(rb_RightGrasperTop.rotation * deltaRotation);

		//Rotates Bottom part of grasper up to zero
		Velocities.z = Mov_Params.GrapserPinchSpeed.z * Input.GetAxisRaw ("RightTrigger");
		deltaRotation = Quaternion.Euler(Velocities * Time.deltaTime);
		rb_RightGrasperBottom.MoveRotation(rb_RightGrasperBottom.rotation * deltaRotation);


	}



	/**
	 * Makes it be that it wont rotate more then the max value, both +-Max value
	*/
	public void ClampAtMax(){
		for(int i = 0;i<3;i++){
			float axisRotation = rb_RightGrasperTop.transform.localRotation.eulerAngles [i];

			//this puts 0 to 180, then -180 to 0
			if (axisRotation > 180) {
				axisRotation -= 360;
			}
			if (Mathf.Abs (axisRotation) > Mov_Params.MaxPincherRotations [i]+0.2) {	//the +0.2 helps it get unstuck from a precision based epsilon bug.
				KeepRotatingOnThisAxis[i] = false;

			}
		}

	}
	public void ClampAtMax2(){
		float axisRotation = debugFloats[0]= rb_RightGrasperBottom.transform.localRotation.eulerAngles [1];	//for some reason it rotates on the y axis...

		//this puts 0 to 180, then -180 to 0
		if (axisRotation > 180) {
			axisRotation -= 360;
		}
		if (Mathf.Abs (axisRotation) > Mov_Params.MaxPincherRotations [1]+0.2) {	//the +0.2 helps it get unstuck from a precision based epsilon bug.
			HingeJoint hj = rb_RightGrasperBottom.GetComponentInChildren<HingeJoint>();
			JointMotor motor = hj.motor;
			motor.targetVelocity = 10f;
			hj.useMotor = false;
			debugFloats [1] = motor.targetVelocity;
		}

	}

	public void SetUpHingeJointsInGrasper(){
		RightBottomGrasperHinge = rb_RightGrasperBottom.GetComponentInChildren<HingeJoint>();

		//Limits_Bottom
		RightBottomGrasperHinge.useLimits = true;
		JointLimits limits = RightBottomGrasperHinge.limits;
			limits.min = -Mov_Params.MaxPincherRotations.z; 
			limits.max=0;
		RightBottomGrasperHinge.limits = limits;
		//MotorBottom:
		RightBottomGrasperHinge.useMotor = true;
		RightBottomGrasperMotor = RightBottomGrasperHinge.motor;
		RightBottomGrasperMotor.targetVelocity = -Mov_Params.GrapserPinchSpeed.z;
		RightBottomGrasperHinge.motor = RightBottomGrasperMotor;


		//Limits top:
		RightTopGrasperHinge = rb_RightGrasperTop.GetComponentInChildren<HingeJoint>();
		RightTopGrasperHinge.useLimits = true;
		limits = RightTopGrasperHinge.limits;
		limits.min = 0; 
		limits.max = Mov_Params.MaxPincherRotations.z;
		RightTopGrasperHinge.limits = limits;

		//Motor top:
		RightTopGrasperHinge.useMotor = true;
		RightTopGrasperMotor = RightTopGrasperHinge.motor;
		RightTopGrasperMotor.targetVelocity = Mov_Params.GrapserPinchSpeed.z;
		RightTopGrasperHinge.motor = RightTopGrasperMotor;


	}

	public void SetUpHingeJointsInGrasper2Hands(){
		RightBottomGrasperHinge = rb_RightGrasperBottom.GetComponentInChildren<HingeJoint>();

		//Limits_Bottom
		RightBottomGrasperHinge.useLimits = true;
		JointLimits limits = RightBottomGrasperHinge.limits;
		limits.min = -Mov_Params.MaxPincherRotations.z; 
		limits.max=0;
		RightBottomGrasperHinge.limits = limits;
		//MotorBottom:
		RightBottomGrasperHinge.useMotor = true;
		RightBottomGrasperMotor = RightBottomGrasperHinge.motor;
		RightBottomGrasperMotor.targetVelocity = -Mov_Params.GrapserPinchSpeed.z;
		RightBottomGrasperHinge.motor = RightBottomGrasperMotor;


		//Limits top:
		RightTopGrasperHinge = rb_RightGrasperTop.GetComponentInChildren<HingeJoint>();
		RightTopGrasperHinge.useLimits = true;
		limits = RightTopGrasperHinge.limits;
		limits.min = 0; 
		limits.max = Mov_Params.MaxPincherRotations.z;
		RightTopGrasperHinge.limits = limits;

		//Motor top:
		RightTopGrasperHinge.useMotor = true;
		RightTopGrasperMotor = RightTopGrasperHinge.motor;
		RightTopGrasperMotor.targetVelocity = Mov_Params.GrapserPinchSpeed.z;
		RightTopGrasperHinge.motor = RightTopGrasperMotor;









		//LEFT HAND:
		LeftBottomGrasperHinge = rb_LeftGrasperBottom.GetComponentInChildren<HingeJoint> ();
		//Limits_Bottom
		LeftBottomGrasperHinge.useLimits = true;
		limits = LeftBottomGrasperHinge.limits;
		limits.min = -Mov_Params.MaxPincherRotations.z; 
		limits.max=0;
		LeftBottomGrasperHinge.limits = limits;
		//MotorBottom:
		LeftBottomGrasperHinge.useMotor = true;
		LeftBottomGrasperMotor = LeftBottomGrasperHinge.motor;
		LeftBottomGrasperMotor.targetVelocity = -Mov_Params.GrapserPinchSpeed.z;
		LeftBottomGrasperHinge.motor = LeftBottomGrasperMotor;


		//Limits top:
		LeftTopGrasperHinge = rb_LeftGrasperTop.GetComponentInChildren<HingeJoint>();
		LeftTopGrasperHinge.useLimits = true;
		limits = LeftTopGrasperHinge.limits;
		limits.min = 0; 
		limits.max = Mov_Params.MaxPincherRotations.z;
		LeftTopGrasperHinge.limits = limits;

		//Motor top:
		LeftTopGrasperHinge.useMotor = true;
		LeftTopGrasperMotor =LeftTopGrasperHinge.motor;
		LeftTopGrasperMotor.targetVelocity = Mov_Params.GrapserPinchSpeed.z;
		LeftTopGrasperHinge.motor =LeftTopGrasperMotor;





	}



	public void UpdateGrasperPinchMotors(){  //using y=2*Max*Input - Max  puedo pasar de [0,1] a [-Max, Max]  equivalente a Y=Max*(2Input-1)
		RightTopGrasperMotor.targetVelocity = Mov_Params.GrapserPinchSpeed.z * (2*Input.GetAxisRaw ("RightTrigger")-1);
		RightTopGrasperHinge.motor = RightTopGrasperMotor;

		RightBottomGrasperMotor.targetVelocity = -Mov_Params.GrapserPinchSpeed.z * (2 * Input.GetAxisRaw ("RightTrigger")-1);
		RightBottomGrasperHinge.motor = RightBottomGrasperMotor;

		LeftTopGrasperMotor.targetVelocity = Mov_Params.GrapserPinchSpeed.z * (2*Input.GetAxisRaw ("LeftTrigger")-1);
		LeftTopGrasperHinge.motor =LeftTopGrasperMotor;

		LeftBottomGrasperMotor.targetVelocity = -Mov_Params.GrapserPinchSpeed.z * (2 * Input.GetAxisRaw ("LeftTrigger")-1);
		LeftBottomGrasperHinge.motor = LeftBottomGrasperMotor;


	}




	/******************************************************************************************************************************************
	 * 			Insertion Methods:  Torsion and Insertion
	 * *****************************************************************************************************************************************/

	void UpdateInsertionTorsion(){

		//RIGHT HAND
		if (rb_RightInsertion == null) {
			Debug.Log ("No rb_RightInsertionRotator rb in MyInputHandler Script on GameObject:"+this.name);
		}
		Quaternion deltaRotation = Quaternion.Euler(new Vector3 (0,-Input.GetAxisRaw("DPadHorizontal"),0)  * Mov_Params.TorsionSpeed);
		//CONTROLS 2: Quaternion deltaRotation = Quaternion.Euler(new Vector3 (0,Input.GetButton("XButton").GetHashCode()-Input.GetButton("BButton").GetHashCode(),0) * Time.deltaTime * Mov_Params.TorsionSpeed);
		rb_RightInsertion.MoveRotation(rb_RightInsertion.rotation * deltaRotation);


		//LEFT HAND
		if (rb_LeftInsertion == null) {
			Debug.Log ("No rb_LeftInsertionRotator rb in MyInputHandler Script on GameObject:"+this.name);
		}
		deltaRotation = Quaternion.Euler(new Vector3 (0,(Input.GetButton("XButton")?1f:0f)-(Input.GetButton("BButton")?1f:0f),0)  * Mov_Params.TorsionSpeed);
		//CONTROLS 2: deltaRotation = Quaternion.Euler(new Vector3 (0,Input.GetAxisRaw("DPadHorizontal"),0) * Time.deltaTime * Mov_Params.TorsionSpeed);
		rb_LeftInsertion.MoveRotation(rb_LeftInsertion.rotation * deltaRotation);


	}


	void UpdateInsertionProfundidad(){
		//rb_RightInsertion.isKinematic = Input.GetAxisRaw ("DPadVertical") != 0;

		rb_RightInsertion.MovePosition (rb_RightInsertion.position + rb_RightInsertion.transform.up*(-Input.GetAxisRaw("DPadVertical")) * Mov_Params.InsertionSpeed );
		rb_LeftInsertion.MovePosition (rb_LeftInsertion.position + rb_LeftInsertion.transform.up*(Input.GetButton("AButton")?1:0-Input.GetButton("YButton").GetHashCode()) * Mov_Params.InsertionSpeed);
	


		if (Input.GetAxisRaw ("DPadVertical") != 0) {
			Debug.Log ("Dpad Insertion = "+Input.GetAxisRaw ("DPadVertical"));
		}



		//Controls 2:
		//rb_RightInsertion.MovePosition (rb_RightInsertion.position + rb_RightInsertion.transform.up * (Input.GetButton("AButton").GetHashCode()-Input.GetButton("YButton").GetHashCode()) * Mov_Params.InsertionSpeed);
		//rb_LeftInsertion.MovePosition (rb_LeftInsertion.position + rb_LeftInsertion.transform.up * (-Input.GetAxisRaw("DPadVertical")) * Mov_Params.InsertionSpeed);


	}
/*
		

			float inputRight,inputLeft=0;

		if (CurrentControlLayout.RightInsertionPlus.Contains ("Button")) {
			inputRight= (Input.GetButton (CurrentControlLayout.RightInsertionPlus).GetHashCode () - Input.GetButton (CurrentControlLayout.RightInsertionMinus).GetHashCode ());
			inputLeft = (-Input.GetAxisRaw (CurrentControlLayout.LeftInsertionPlus));
		} else {
			inputRight = (-Input.GetAxisRaw (CurrentControlLayout.RightInsertionPlus));
			inputLeft= (Input.GetButton (CurrentControlLayout.LeftInsertionPlus).GetHashCode () - Input.GetButton (CurrentControlLayout.LeftInsertionMinus).GetHashCode ());
		}


		rb_RightInsertion.MovePosition (rb_RightInsertion.position + rb_RightInsertion.transform.up * inputRight * Mov_Params.InsertionSpeed);
		rb_LeftInsertion.MovePosition (rb_LeftInsertion.position + rb_LeftInsertion.transform.up*(-inputLeft) * Mov_Params.InsertionSpeed);




	
	public int ControllerLayout=0;
	public XboxInputLayout[] ControlLayouts;
	public XboxInputLayout  CurrentControlLayout;

	[System.Serializable]
	public class XboxInputLayout{
		public string RightInsertionPlus="DPadVertical",
		RightInsertionMinus= "DPadVertical",
		RightTorsionPlus ="DPadHorizontal",
		RightTorsionMinus ="DPadHorizontal",
		RightPositionX = "RightJoystickHorizontal",
		RightPositionZ = "RightJoystickVertical",
		RightGrasperPinch="RightTrigger",

		LeftInsertionPlus="AButton",
		LeftInsertionMinus= "YButton",
		LeftTorsionPlus ="XButton",
		LeftTorsionMinus ="BButton",
		LeftPositionX = "LeftJoystickHorizontal",
		LeftPositionZ = "LeftJoystickVertical",
		LeftGrasperPinch="LeftTrigger";

	}

	public void UpdateControlLayout(){
		if (Input.GetButton ("BackButton")) {
			CurrentControlLayout= ControlLayouts [++ControllerLayout%ControlLayouts.Length]; 
		}
	}

	*/


}
