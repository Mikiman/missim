﻿using UnityEngine;
using System.Collections;

public class DualRingLink : MonoBehaviour,ObjInSceneEditPlayInterface {


	public Rigidbody ring1, ring2;
	public DontGoThroughThings dgtt1, dgtt2;


	#region ObjInSceneEditPlayInterface implementation
	public void OnEnterPlayMode ()
	{
		//gameObject.GetComponent<Rigidbody> ().isKinematic=false;
		ring1.isKinematic=false;
		ring2.isKinematic=false;
		dgtt1.enabled = true;
		dgtt2.enabled = true;



	}
	public void OnEnterEditMode ()
	{
		ring1.isKinematic=true;
		ring2.isKinematic=true;
		dgtt1.enabled = false;
		dgtt2.enabled = false;

	}
	#endregion
}
