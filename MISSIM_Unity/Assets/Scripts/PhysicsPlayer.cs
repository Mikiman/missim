﻿using UnityEngine;
using System.Collections;

public class PhysicsPlayer : MonoBehaviour {

	public float MoveForce = 10f;
	public float JumpForce = 3f;
	public float maxYVelocity = 10f;
	[SerializeField]private Vector3 Velocity;

	private Rigidbody rb;
	private bool jump;

	void Start(){
		rb = GetComponent<Rigidbody> ();
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (rb == null) {
			Debug.Log ("there is no rigidbody on the PhysicsPlayer");
			return;
		}
		float y=0;
		if (jump && rb.velocity.y < maxYVelocity) {
			y = JumpForce;
		}
		Vector3 Force = new Vector3 (Input.GetAxisRaw ("Horizontal"), y, Input.GetAxisRaw ("Vertical")) * MoveForce;
		rb.AddForce (Force, ForceMode.Acceleration);
		Velocity = rb.velocity;
	}

	void Update(){
		jump = Input.GetKey(KeyCode.Space);
	}

}
