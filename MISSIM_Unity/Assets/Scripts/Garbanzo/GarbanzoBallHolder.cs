﻿using UnityEngine;
using System.Collections;

public class GarbanzoBallHolder : MonoBehaviour {

	public int holderId;
	//public Color correctColor,incorrectColor,neutralColor;
	public ParticleSystem correctPartSystem, wrongPartSystem;
	public GarbanzoBallZone triggerZone;

	public bool complete;

	// Use this for initialization
	void Start () {
		triggerZone.init (this);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnBallEnteredZone(GarbanzoBall ball){
		int ballId = ball.id;

		bool correctZone = CheckIds (ballId);

		if (correctZone) {
			Debug.Log ("Ball and Zone do match types!");
			complete = true;
			correctPartSystem.Play ();
		}
		else{
			Debug.Log ("Ball and Zone dont match types");
			wrongPartSystem.Play ();
		}
	}


	public bool CheckIds(int ballId){
		if(holderId==0||ballId==0||ballId==holderId){
			return true;
		}	
		return false;
	}


}
