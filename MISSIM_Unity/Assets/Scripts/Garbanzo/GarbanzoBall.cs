﻿using UnityEngine;
using System.Collections;

public class GarbanzoBall : MonoBehaviour,ObjInSceneEditPlayInterface {

	public int id; //0 any, 1,2,3... differnet color

	#region ObjInSceneEditPlayInterface implementation
	public void OnEnterPlayMode ()
	{
		gameObject.GetComponent<Rigidbody> ().isKinematic=false;

	}
	public void OnEnterEditMode ()
	{
		gameObject.GetComponent<Rigidbody> ().isKinematic=true;
	}
	#endregion
}
