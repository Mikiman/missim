﻿using UnityEngine;
using System.Collections;

public class GarbanzoBallZone : MonoBehaviour {

	private GarbanzoBallHolder holder;
	public void init(GarbanzoBallHolder gbh){
		holder = gbh;
	}


	private void OnTriggerEnter(Collider other)
	{
		GarbanzoBall ball = other.gameObject.GetComponent<GarbanzoBall> ();
		if (ball == null) {
			return;
		}

		holder.OnBallEnteredZone (ball);

	}
		


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
