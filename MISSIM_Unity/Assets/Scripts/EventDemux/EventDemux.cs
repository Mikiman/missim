﻿using UnityEngine;
using System.Collections;
using TFG.Data;
using TFG;

	/*******************************
		 * Event Meanings
		 *******************************/
using System;
using System.Collections.Generic;


	public enum MyEventMeanings
	{
		ClickedOnItemInScene,// = 0,
		SetTransform,//=1,
		AddObjectToSceneByPrefabId,//=2,
		RemoveObjectFromScene,//=3,
		SavePathChanged,//=4,
		Save,//=5,
		Load,//=6,
		SetToolbarTool,
		SetToolbarToolMode,
		ToggleToolBarGO,
		EditToPlay,
		PlayToEdit,
		TitleToEdit,
		TitleToPlay,
		DeleteAllAndReturnToMenu
			
	}




	public class EventDemux : MonoBehaviour {
		/*******************************
		 * SINGLETON
		 *******************************/
		public static EventDemux instance = null;  
		void Awake()
		{
			//Check if instance already exists
			if (instance == null) {instance = this;} 
			else if (instance != this) {Destroy (gameObject);}  
			//Sets this to not be destroyed when reloading scene
			DontDestroyOnLoad(gameObject);



		//BAD! but i dont know how to do better, it keeps getting erased, SOLVED BY function SetEditorWindowScript() called in EditorWindow.Initialize()
			/**
			 * GameObject EdWindow = GameObject.Find ("EditorWindow (EdWindow)");
			if (EdWindow != null) {
				editorWindowScript = EdWindow.GetComponent<EditorWindow>();
			}
			*/
			//BAD! but i dont know how to do better, it keeps getting erased
		sceneDataHolder = (GameObject.FindGameObjectWithTag ("SceneDataHolder")).GetComponent<SceneDataHolder>();
			


		}
			


		/*******************************
		 * HANDLER References go here:
		 *******************************/
	public Root rootUIScript;
	public EditorWindow editorWindowScript;
	public SceneDataHolder sceneDataHolder;
	public ToolBarController toolBarController;


	#region Temporal, Do something better!
	//APAÑO TEMPORAL: Show and hide xbox controller on push of button.
	public Transform XboxControllerObject;
	public Vector3 xboxControllerVisiblePostion, xboxControllerNotVisiblePosition; 
	public void ToggleXboxRemoteVisibility(){
		if (XboxControllerObject.position == xboxControllerVisiblePostion) {
			XboxControllerObject.position = xboxControllerNotVisiblePosition;
		} else {
			XboxControllerObject.position = xboxControllerVisiblePostion;

		}
	}
	//APAÑO TEMPORAL: DUAL Input angles
	public GameObject[] InputObjects;
	public int currentInput;
	public void SwitchInputAngle(){
		InputObjects [currentInput].SetActive (false);
		currentInput = currentInput == 0 ? 1 : 0;
		InputObjects [currentInput].SetActive (true);
	}
	public void ToggleVisibleInputHandler(){
		InputObjects [currentInput].SetActive (!InputObjects [currentInput].activeSelf);
	}
	public void SetVisibleInputHandler(bool visible){
		InputObjects [currentInput].SetActive (visible);
	}

	public void RestartPlayMode(){
		//PLAY TO EDIT 
		sceneDataHolder.PlayModeToEditMode (editorWindowScript.FullSavePath); //deletes objects and refills scene
		toolBarController.TurnOnGO ();
		sceneDataHolder.EditModeToPlayMode (editorWindowScript.FullSavePath);

		
	}

	#endregion


	/// <summary>
	/// Sets the editor window script.
	/// Called on by EditorWindow.cs in Initialize
	/// </summary>
	/// <param name="script">Script.</param>
	public void SetEditorWindowScript(EditorWindow script){
		editorWindowScript = script;
	}

	/*
	Dictionary<Type, Delegate> typeProcessorMap = new Dictionary<Type, Delegate>
	{
		//{ typeof(int), new Action<int>(i => { /* do something with i  }) },
		//{ typeof(string), new Action<string>(s => { /* do something with s  }) },
		{ typeof(EditorWindow), new Action<EditorWindow>(handler => { editorWindowScript=handler; }) },
		{ typeof(SceneDataHolder), new Action<SceneDataHolder>(handler => { sceneDataHolder=handler; }) },

	};

	public void RegisterHandler(object o)
	{
		var t = o.GetType();
		typeProcessorMap[t].DynamicInvoke(o); // invoke appropriate delegate
	}
*/

	public void RegisterHandler<T>(T handlerClass){

		if (typeof(T) == typeof(EditorWindow)) {
			editorWindowScript = handlerClass as EditorWindow;
		}
		else if (typeof(T) == typeof(SceneDataHolder)) {
			sceneDataHolder = handlerClass as SceneDataHolder;
		}
		else if (typeof(T) == typeof(ToolBarController)) {
			toolBarController = handlerClass as ToolBarController;
		}
		else if (typeof(T) == typeof(Root)) {
			rootUIScript = handlerClass as Root;
		}

	}




		/*******************************
		*Funcions:
		*******************************/
		/// <summary>
		/// Reports the event to demux
		/// Called by any script, sending an event and the data necesary to be processed.
		/// </summary>
		/// <param name="myEvent">MyEventMeaning: what type of event it is</param>
		/// <param name="data">Data to process by the handlers.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public void ReportEventToDemux<T>(MyEventMeanings myEvent,T data){
			HandleEvent (myEvent, data);
			
		}

		private void HandleEvent<T>(MyEventMeanings myEvent,T data){
			switch (myEvent) {
			/*case MyEventMeanings.:
				Debug.Log ("clickedOnObjectInEditor:" + data.GetType ());
				(data as Testing_EventDemux).PrintTestString();
				break;
			*/
		case MyEventMeanings.ClickedOnItemInScene:
				//Debug.Log ("ClickedOnItemInScene:" + data.GetType ());
				//editorWindowScript.SelectedObject = data as GameObject;
			editorWindowScript.SetSelectedObject (data as GameObject);
			toolBarController.gameObject.SetActive (true);
			toolBarController.SetSelectedTransform((data as GameObject).transform);


			break;

		case MyEventMeanings.SetTransform:
				//Debug.Log ("ClickedOnItemInScene:" + data.GetType ());
			MyTransformData dataTransform = data as MyTransformData;
			/*
			//UPDATE the Transform of the selectedObject
			editorWindowScript.SelectedObject.transform.position = dataTransform.position;
			editorWindowScript.SelectedObject.transform.rotation = dataTransform.rotation;
			editorWindowScript.SelectedObject.transform.localScale = dataTransform.localScale;

			//Update the DATA of the selectedObject
			TFG.Data.GameObjectInSceneData objectData= editorWindowScript.SceneDataHolder.objectsInScene [2].GetComponent<GameObjectInSceneDataHolder> ().gameObjectInSceneData;
				objectData.position = dataTransform.position;
				objectData.rotation = dataTransform.rotation;
				objectData.scale = dataTransform.localScale;


			*/
			editorWindowScript.SetTransform (dataTransform);
			break;


		case MyEventMeanings.AddObjectToSceneByPrefabId:

			sceneDataHolder.AddObjectToSceneByPrefabId (Convert.ToInt32(data));
			break;



		case MyEventMeanings.RemoveObjectFromScene:

			Debug.Log ("Remove Object with index="+Convert.ToInt32 (data));
			sceneDataHolder.DestroyGameObject (Convert.ToInt32(data));
			break;

		case MyEventMeanings.SavePathChanged:
			editorWindowScript.SetSavePath (data as SavePathData);
			break;

		case MyEventMeanings.Save:

			string fullPathToSave = editorWindowScript.FullSavePath;//editorWindowScript.SavePath + "/" + editorWindowScript.SaveFileName + ".json";
			sceneDataHolder.SaveAll (fullPathToSave);//data.ToString ()); //full path
			break;

		case MyEventMeanings.Load:
			string fullPathToLoad = data.ToString ();// + ".json";
			editorWindowScript.FullSavePath = fullPathToLoad;
			sceneDataHolder.LoadAll (fullPathToLoad);//data.ToString ()); //full path
			break;
		case MyEventMeanings.TitleToPlay:
			string fullPathToLoad1 = data.ToString ();// + ".json";
			editorWindowScript.FullSavePath = fullPathToLoad1;
			sceneDataHolder.TitleMenuToPlayMode (fullPathToLoad1);//data.ToString ()); //full path
			SetVisibleInputHandler (true);//TODO REMOVE THIS

			break;
		case MyEventMeanings.TitleToEdit:
			string fullPathToLoad2 = data.ToString ();// + ".json";
			editorWindowScript.FullSavePath = fullPathToLoad2;
			sceneDataHolder.TitleMenuToEditMode (fullPathToLoad2);//data.ToString ()); //full path

			SetVisibleInputHandler (false);//TODO REMOVE THIS
			break;

		case MyEventMeanings.SetToolbarTool:
			int toolIndex = Convert.ToInt32(data);
			toolBarController.SetTool (toolIndex);
			break;
		case MyEventMeanings.SetToolbarToolMode:
			int toolIndexMode = Convert.ToInt32(data);
			toolBarController.SetToolMode (toolIndexMode);
			break;
		case MyEventMeanings.ToggleToolBarGO:
			bool on = Convert.ToBoolean (data);
			if (on) {
				toolBarController.TurnOnGO ();
			} else {
				toolBarController.TurnOffGO ();
			}
			break;

		case MyEventMeanings.EditToPlay:
			
			String fullPathToSaveTo = editorWindowScript.FullSavePath;
			Debug.Log ("EditToPlay should save to: " + fullPathToSaveTo);
			if (fullPathToSaveTo == "") {
				editorWindowScript.ClickedButton_Save ();
			} else {
				rootUIScript.ToPlaySceneWindow ();
				sceneDataHolder.EditModeToPlayMode (fullPathToSaveTo);
				toolBarController.TurnOffGO ();
				//UnityEngine.Physics.gravity = new Vector3(0f,-9.81f,0f);
				SetVisibleInputHandler (true);//TODO REMOVE THIS

			}
			break;

		case MyEventMeanings.PlayToEdit:
			string fullPathToLoadFrom = editorWindowScript.FullSavePath;
			Debug.Log ("PlayToEdit is going to try and load: "+fullPathToLoadFrom);
			if (fullPathToLoadFrom == "") {
				
			} else {
				rootUIScript.ButtonDown_ToCreateSceneWindow ();
				sceneDataHolder.PlayModeToEditMode (fullPathToLoadFrom); //deletes objects and refills scene
				toolBarController.TurnOnGO ();
				//UnityEngine.Physics.gravity = Vector3.zero;
				SetVisibleInputHandler (false);//TODO REMOVE THIS

			}
			break;

		case MyEventMeanings.DeleteAllAndReturnToMenu:
			sceneDataHolder.DeleteAll (); //deletes all objects
			toolBarController.TurnOffGO();
			SetVisibleInputHandler (false);//TODO REMOVE THIS

			//rootUIScript.ButtonDown_ToCreateMainView();
			break;
		default:
				break;
			}
		}
			









	}
