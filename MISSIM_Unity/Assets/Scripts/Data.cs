﻿using UnityEngine;
using System.Collections;
using ViewModels;
using System.Collections.Generic;
using MarkLight;

namespace TFG.Data{



	[System.Serializable]
	public class SceneData{
		public string sceneName;
		public GameObjectInSceneData[] GOsInScene;
		public MetricViewData [] GlobalMetrics;

		public void Clear(){
			GOsInScene = new GameObjectInSceneData[0];
		}
	}

	[System.Serializable]
	public class GameObjectInSceneData{
		public int indexInScene;
		public int PrefabID; 

		public Vector3 position;
		public Quaternion rotation;
		public Vector3 scale = Vector3.one;

		public TaskObjectData taskObjectData;
	}

	public class _GameObjectInSceneData:ViewField<GameObjectInSceneData>{}

	public class _TaskObjectData:ViewField<TaskObjectData>{}

	[System.Serializable]
	public class TaskObjectData{

		public string taskName;
		public string iconPath;
		//public ListMetricViews listMetricViews;
		public MetricViewData[] metricViewData;

		public TaskObjectData GetCopy(){
			return new TaskObjectData{ taskName = taskName, iconPath = iconPath, metricViewData = metricViewData };
		}

	}

	[System.Serializable]
	public class MetricViewData {
		public string metricDescriptionText;
		public bool metricEnabled;

		public bool MyActive=false; //if visible metric, this should not be here... =(
	}

	[System.Serializable]
	public class MyTransformData{
		public Vector3 position,scale;
		public Quaternion rotation;

		public bool setPos,setRot,setScale,setAll;
	}


	[System.Serializable]
	public class SavePathData{
		public string path,filename,fullPath;
	}

}