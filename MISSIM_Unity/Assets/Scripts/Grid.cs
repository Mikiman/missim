﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Dimensions{
	public Vector3 NumberOfCells= new Vector3(0f,0f,0f);//columns=x, levels = y, rows=z
	public Vector3 SizeOfCells=new Vector3(1f,1f,1f); //x= ancho, y = alto, z= depth 
	public Vector3 SpaceBetweenCells = Vector3.zero;
}

public class Grid : MonoBehaviour {
	public bool CreateGridOnStart = true;		 //Sometimes i might want to just create the grid once.
	public bool OffsetToBottomLeftCorner = true; //necesary so objects dont appear in the middle of the floor
	public bool CellRenderersVisible = true;
	public bool InTestMode = true;

	public Transform CellPrefab;
	public Transform TestPrefab;		//TODO Delete this from the scritpt
	public Vector3 TestCreatePrefabHere; //TODO Delete this from the scritpt
	public Dimensions dimensions;

	public Transform[,,] CellsArray;


	// Use this for initialization
	void Start () {
		if (CreateGridOnStart) {
			CreateGrid ();	
		}



	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.G)){
			toggleGridCellRenderers ();
		}
		if(Input.GetKeyDown(KeyCode.H)){
			DestroyAndRecreateGrid ();
		}


	}



	private void CreateGrid(){

		CellsArray = new Transform[(int)dimensions.NumberOfCells.x, (int)dimensions.NumberOfCells.y,(int) dimensions.NumberOfCells.z];

		//necesary so objects dont appear in the middle of the floor
		Vector3 Offset = new Vector3 (dimensions.SizeOfCells.x / 2, dimensions.SizeOfCells.y / 2, dimensions.SizeOfCells.z / 2);
		Vector3 InstantiatePos;

		for (int x = 0; x < dimensions.NumberOfCells.x; x++) {
			for (int y = 0; y < dimensions.NumberOfCells.y; y++) {
				for (int z = 0; z < dimensions.NumberOfCells.z; z++) {

					InstantiatePos = new Vector3 (
						transform.position.x + x * (dimensions.SizeOfCells.x + dimensions.SpaceBetweenCells.x),
						transform.position.y + y * (dimensions.SizeOfCells.y + dimensions.SpaceBetweenCells.y),
						transform.position.z + z * (dimensions.SizeOfCells.z + dimensions.SpaceBetweenCells.z)
					);

					if (OffsetToBottomLeftCorner) {
						InstantiatePos += Offset;
					}
						
					Transform cell = (Transform)Instantiate (
							CellPrefab, 
							InstantiatePos, 
							Quaternion.identity
					);

					CellsArray [x, y, z] = cell;

					cell.parent = transform;
					cell.name = string.Format (cell.name+" at ({0},{1},{2})",x,y,z);
					cell.localScale = Vector3.Scale(cell.localScale,dimensions.SizeOfCells); //Producto escalar to size prefab to match cell size.

					GridCell Script_gridCell = cell.GetComponent<GridCell> ();
					Script_gridCell.Position = InstantiatePos;

					if (InTestMode  && x == 10 && y==0 && z == 5) {
						Transform element = (Transform)Instantiate (TestPrefab, InstantiatePos, Quaternion.identity);
						Script_gridCell.SetElement (element,true);
						//element.parent = cell;
						//element.localScale = Vector3.one;
						//element.localPosition =new Vector3 (0, -0.5f, 0);
					}

					Script_gridCell.SetRendererVisible (CellRenderersVisible);


				}
			}
		}

		//TODO remove this eventually
		if (InTestMode) {
			//Creates in a specific position given as parameter.
			CreateObjectInThisCell (TestPrefab, TestCreatePrefabHere);

			//creates in a random cell in the grid
			Vector3 randomIntegerPos = new Vector3 ((int)Random.Range (0, dimensions.NumberOfCells.x), (int)Random.Range (0, dimensions.NumberOfCells.y), (int)Random.Range (0, dimensions.NumberOfCells.z));
			//this causes an error: dont do it!!  //randomIntegerPos = Vector3.Scale (randomIntegerPos,dimensions.SizeOfCells);
			CreateObjectInThisCell (TestPrefab, randomIntegerPos);
			//Debug.Log ("shold have placed in random position: "+randomIntegerPos.ToString());
		}
	}


	/**
		Creates a given prefab in a given cell.
		returns true if it can be placed, returns false if cellsArry is null or position is not in the Grid.

		NOTE: the position has to be 10 not 50 even if the size ofthe cell is 5. 
	*/
	public bool CreateObjectInThisCell(Transform prefab, Vector3 cellPos){
		//First i need to make sure the grid exists
		if (CellsArray == null) {
			return false;
		}

		//Second i need to clamp float to int
		cellPos.x = Mathf.Floor (cellPos.x);
		cellPos.y = Mathf.Floor (cellPos.y);
		cellPos.z = Mathf.Floor (cellPos.z);

		//Then Make sure the position is in the Grid
		if(cellPos.x>= dimensions.NumberOfCells.x ||cellPos.y>= dimensions.NumberOfCells.y ||cellPos.z>= dimensions.NumberOfCells.z){
			return false;
		}

		Transform element = (Transform)Instantiate (prefab, cellPos, Quaternion.identity);
		GridCell Script_gridCell = CellsArray[(int)cellPos.x,(int)cellPos.y,(int)cellPos.z].GetComponent<GridCell> ();
		Script_gridCell.SetElement (element,true);
		
		return true;
	}


	public void toggleGridCellRenderers(){
		CellRenderersVisible = !CellRenderersVisible;
		foreach(Transform cell in CellsArray){
			cell.GetComponent<GridCell> ().SetRendererVisible(CellRenderersVisible);
		}
	}


	public void DestroyAndRecreateGrid(){
		foreach (Transform cell in CellsArray) {
			Destroy (cell.gameObject);
		}
		CellsArray = null;
		CreateGrid ();

	}

}
