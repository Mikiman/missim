﻿using UnityEngine;
using System.Collections;

public class LoopTottem : MonoBehaviour {


	public Transform Loop;
	public MeshRenderer ColorBase;
	public Material CorrectMaterial, IncorrectMaterial,NormalMaterial;

	private Transform Peg;
	//For the peg entery and exiting the loop
	private  Vector3 EntryPoint;
	private Vector3 ExitPoint;
	private Vector3 TrajectoryVector;	//The vector indicating the direction of the pegs trajectory into and out of the loop

	private Vector3 LoopForwardVector;	//the vector of the loops normal plane
	private Vector3 LoopOnXZPlaneVector;//the projection of the vector of the loops normal plane onto xz plane
	private Vector3 TrajectoryVectorOnXZ;//the projectotion of the TrajectoryVector onto the xz plane

	//angle between LoopOnXZPlaneVector and TrajectoryVectorOnXZ, both on the xz plane.
	private float Angle;

	//90º would be the theoretically correct answer. i choose 70º cause its what makes it so peg doesnt go through ring. it would hit the ring.
	//maybe say, if between 70 and 90, youget a enalty for hitting the ring?
	public float AcceptedAngleRange = 70; //number of degrees that are exceptad as in correct direction


	//True if the peg went through the loop in the correct way (true if |Angle| <= AcceptedAngleRange)
	[SerializeField] private bool CorrectDirection;


	public void Calculate(){
		LoopForwardVector = Loop.forward;
		TrajectoryVector = ExitPoint - EntryPoint;

		//LoopOnXZPlaneVector = new Vector3(LoopForwardVector.x,0f,LoopForwardVector.z);
		//TrajectoryVector = new Vector3(TrajectoryVector.x,0f,TrajectoryVector.z);
	
		Angle = Vector3.Angle (LoopForwardVector, TrajectoryVector);

		//Abs makes it be able to come form +-90º
		if (Mathf.Abs (Angle) <= AcceptedAngleRange) {
			CorrectDirection = true;
			Debug.Log ("Peg Went through Correctly");
			ColorBase.material = CorrectMaterial;
			Invoke ("EraseBaseColorMaterial", 3f);
		} else {
			CorrectDirection = false;
			Debug.Log ("Peg Went through Incorrectly");
			ColorBase.material = IncorrectMaterial;
			Invoke ("EraseBaseColorMaterial", 3f);
		}
		//Peg.transform.parent = null;
		Peg = null;


	}



	public void SetPeg(Transform peg){
		Peg = peg;
		//Peg.SetParent (this.transform);
	}


	public void SetEntryPoint(Vector3 entry){
		EntryPoint = entry;
	}
	public void SetExitPoint(Vector3 exit){
		ExitPoint = exit;
	}

	public void EraseBaseColorMaterial()
	{
		ColorBase.material = NormalMaterial;
	}


}
