﻿using UnityEngine;
using System.Collections;

public class GrasperRotator : MonoBehaviour {


	//SEE: https://docs.unity3d.com/ScriptReference/Rigidbody.MoveRotation.html

	public Vector3 eulerAngleVelocity;
	public Rigidbody rb1;
	public Rigidbody rb2;
	public bool[] KeepRotatingOnThisAxis = { true, true, true };

	public Vector3 MaxRotations = new Vector3 ();
	public const int Yaw = 2,Torsion = 1,Ptich =0;

	public float DebugFloat;


	void Start() {
		rb1 = transform.GetChild(0).GetComponent<Rigidbody>();
		rb2 = transform.GetChild(1).GetComponent<Rigidbody>();
	}
	void FixedUpdate() {
		if (rb1 == null) {
			Debug.Log ("there is no RigidBody on the gameobject with the Physics Rotator Script");		
			return;
		}

		float z = rb1.rotation.eulerAngles.z;
		DebugFloat = z;




			ClampAtMax ();
			SimpleRotate ();


	}

	private void SimpleRotate(){
		
		Vector3 Velocities = eulerAngleVelocity;

		for (int i = 0; i < 3; i++) {
			if (!KeepRotatingOnThisAxis [i]) {
				Velocities [i] = 0;
				KeepRotatingOnThisAxis [i] = true;	//reset values for next framestep
			}
		}

		Velocities.z = eulerAngleVelocity.z * Input.GetAxisRaw ("Horizontal");
		Quaternion deltaRotation = Quaternion.Euler(Velocities * Time.deltaTime);
		rb1.MoveRotation(rb1.rotation * deltaRotation);

		Velocities.z = eulerAngleVelocity.z * -Input.GetAxisRaw ("Horizontal");
		deltaRotation = Quaternion.Euler(Velocities * Time.deltaTime);
		rb2.MoveRotation(rb2.rotation * deltaRotation);


	}



	/**
	 * Makes it be that it wont rotate more then the max value, both +-Max value
	*/
	public void ClampAtMax(){
		for(int i = 0;i<3;i++){
			float axisRotation = rb1.transform.localRotation.eulerAngles [i];


			//this puts 0 to 180, then -180 to 0
			if (axisRotation > 180) {
				axisRotation -= 360;
			}
			if (Mathf.Abs (axisRotation) > MaxRotations [i]+0.2) {	//the +0.2 helps it get unstuck from a precision based epsilon bug.
				//eulerAngleVelocity[i] *= -1;		//swings oopposite direction
				//eulerAngleVelocity[i] = 0;		//stops it
				KeepRotatingOnThisAxis[i] = false;
			}
		}

	}


}
