﻿using UnityEngine;
using System.Collections;

public class Metrica_DistanceTravelled : MonoBehaviour {

	public float distanceTravelled;
	public  Vector3 lastPos;



	// Use this for initialization
	void Awake () {
		lastPos = transform.position;
	}


	
	// Update is called once per frame
	void Update () {
		//if (lastPos != transform.position) {
			CalculateDistance ();
		//}
	}

	void CalculateDistance(){
		distanceTravelled += Vector3.Magnitude (lastPos - transform.position);
		lastPos = transform.position;
	}
}
