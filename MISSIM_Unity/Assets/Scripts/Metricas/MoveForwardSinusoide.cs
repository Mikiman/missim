﻿using UnityEngine;
using System.Collections;

public class MoveForwardSinusoide : MonoBehaviour {
	public PathStyle style = PathStyle.straight;
	public float sinusoidAmplitude=1f;
	public float sinusoidFrecuency = 0.2f;
	public bool Move= true;
	public float Speed=1f;



	// Use this for initialization
	void Start () {}

	// Update is called once per frame
	void Update () {
		if (Move) {
			SetNewForwardDirection ();
			transform.position += transform.forward * Time.deltaTime*Speed;	//BLue Arrow
		}
	}





	void SetNewForwardDirection(){
		switch(style){
			case PathStyle.straight:
				break;
			case PathStyle.sinusoide:
			transform.forward = new Vector3(sinusoidAmplitude*Mathf.Sin (Time.time*2*Mathf.PI*sinusoidFrecuency),transform.forward.y,transform.forward.z);
				break;
			case PathStyle.cosign:
			transform.forward = new Vector3(sinusoidAmplitude*Mathf.Cos (Time.time*2*Mathf.PI*sinusoidFrecuency),transform.forward.y,transform.forward.z);
			break;
		}
	
	}



}

public enum PathStyle {straight,sinusoide,cosign}