﻿using UnityEngine;
using System.Collections;

public class MoveForward : MonoBehaviour {

	public bool Move= true;
	public float Speed=1f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Move) {
			transform.forward = new Vector3(transform.forward.x,transform.forward.y,transform.forward.z);
			transform.position += transform.forward * Time.deltaTime*Speed;	//BLue Arrow
		}
	}
}
