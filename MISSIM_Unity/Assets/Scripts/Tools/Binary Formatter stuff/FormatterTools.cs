﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO; //filestream

public static class FormatterTools {

	/// <summary>
	/// Saves data to a file.
	/// </summary>
	/// <param name="data">Data.</param>
	/// <param name="path">Path.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static void SaveToFile<T>(T data,string path){
		//Step 1: create formater
		BinaryFormatter bf = new BinaryFormatter ();

		//Step 2: create the file and open it.
		//this is where the data is going to be saved.
		FileStream file = File.Create (Application.dataPath + path);
//		Debug.Log("saved To: "+ Application.persistentDataPath + pathExtensionString);


		//Step 3: Create a data container and fill it.
		//what data are we going to save:
		//DO NOT serialize a Monodevelop. leads to wierd things...  

		//using a constructor method to fill data container.
		//PlayerStatsData data = new PlayerStatsData(GetComponent<PlayerStatsController>().statsData);

		/*You can use a constructor like before or use setters like this if youve written them.
        data.health = health;
        data.experience = experience;
        data.setHealth(health);
        */
		Debug.Log("data to save = "+data);

		//Step 4: serialize the dataContainer into the file and then close the file.
		bf.Serialize(file, data);
		file.Close();

		Debug.Log("saved file closed succesfully");
		//one solution for web: serialize into a string and then pass the string to playerprefs. and then send over internet.



	}

	public static void SaveToFile(string jsonString,string path){
		//Step 1: create formater
		BinaryFormatter bf = new BinaryFormatter ();

		//Step 2: create the file and open it.
		//this is where the data is going to be saved.
		FileStream file = File.Create (Application.dataPath + path);
		//		Debug.Log("saved To: "+ Application.persistentDataPath + pathExtensionString);


		//Step 3: Create a data container and fill it.
		//what data are we going to save:
		//DO NOT serialize a Monodevelop. leads to wierd things...  

		//using a constructor method to fill data container.
		//PlayerStatsData data = new PlayerStatsData(GetComponent<PlayerStatsController>().statsData);

		/*You can use a constructor like before or use setters like this if youve written them.
        data.health = health;
        data.experience = experience;
        data.setHealth(health);
 		*/
		Debug.Log("data to save = "+jsonString);

		//Step 4: serialize the dataContainer into the file and then close the file.
		bf.Serialize(file, new Dumbdata(jsonString.ToString()));
		file.Close();

		Debug.Log("saved file closed succesfully");
		//one solution for web: serialize into a string and then pass the string to playerprefs. and then send over internet.


	}



	public static T LoadFromFile <T>(string path){
		//Step 1: Check if the file exists.
		if (File.Exists(Application.dataPath + path)) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.dataPath + path,FileMode.Open);

			//its necesary to cast the generic object into the playerdatacontainer specifically.
			T data = (T)bf.Deserialize(file);

			file.Close();

			Debug.Log("unload finished succesfully");
			return data;

		}
		return default(T);
	}

	public static string LoadFromFile(string path){
		//Step 1: Check if the file exists.
		if (File.Exists(Application.dataPath + path)) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.dataPath + path,FileMode.Open);

			//its necesary to cast the generic object into the playerdatacontainer specifically.
			Dumbdata data = (Dumbdata)bf.Deserialize(file);

			file.Close();

			Debug.Log("unload finished succesfully");
			return data.jsonString;

		}
		return "File Doesnt exist";
	}


}
	

/*


[System.Serializable]
class Dumbdata
{
	public string jsonString;
	public Dumbdata(string json){
		jsonString = json;
	}
}
*/
