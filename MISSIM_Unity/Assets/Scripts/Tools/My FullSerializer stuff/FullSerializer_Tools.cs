﻿using System;
using FullSerializer;


public static class FullSerializer_Tools {

	private static readonly fsSerializer _serializer = new fsSerializer();

	/// <summary>
	/// Serialize the specified type and value.
	/// returns a json string
	/// USE: 		
	/// string json = FullSerializer_Tools.Serialize(typeof(testSaveMeClass),new testSaveMeClass(path,Vector3.one*3));
	/// </summary>
	/// <param name="type">Type.</param>
	/// <param name="value">Value.</param>
	public static string Serialize(Type type, object value) {
		// serialize the data
		fsData data;
		_serializer.TrySerialize(type, value, out data).AssertSuccessWithoutWarnings();

		// emit the data via JSON
		return fsJsonPrinter.CompressedJson(data);
	}

	public static object Deserialize(Type type, string serializedState) {
		// step 1: parse the JSON data
		fsData data = fsJsonParser.Parse(serializedState);

		// step 2: deserialize the data
		object deserialized = null;
		_serializer.TryDeserialize(data, type, ref deserialized).AssertSuccessWithoutWarnings();

		return deserialized;
	}
}
