﻿using UnityEngine;
using System.Collections;
using LitJson;		//for json
using System.IO;	//for file system

public class WriteToJson : MonoBehaviour {

	public Character player = new Character(0,"miki",100,false,new int[]{0,200,30,100});
	JsonData playerJson;




	// Use this for initialization
	void Start () {
		//playerJson = JsonMapper.ToJson(player); 
		//Debug.Log(playerJson);
		//File.WriteAllText (Application.dataPath+"/Resources/WriteToPlayerJSON.json",playerJson.ToString());//opens,writes,closes   Overwrites existing file.

		JSON_Tools.SaveToJSONFile(player,"/Resources/WriteToPlayerJSON.json");
	}
	

}



public class Character{
	public int id;
	public string name;
	public int health;
	public bool aggressive;
	public int[] stats; //power,speed,accuracy

	public Character(int id,string name,int health,bool aggresive,int[] stats){
		this.id = id;
		this.name = name;
		this.health = health;
		this.aggressive = aggresive;
		this.stats = stats;
	}
}
