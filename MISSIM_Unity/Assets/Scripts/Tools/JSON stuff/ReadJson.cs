using UnityEngine;
using System.Collections;
using System.IO; 	//for file
using LitJson;		//For Json Parser methods

public class ReadJson : MonoBehaviour{
	private string jsonString;
	private JsonData itemData; //holds the json object.  thanks to LitJson

	void Start(){
		//Get the String
		jsonString = File.ReadAllText(Application.dataPath + "/Resources/Items.json"); // opens, reads and closes file
		//jsonString = File.ReadAllText(Application.dataPath + "/Resources/Strings.json"); // opens, reads and closes file


		//Debug.Log(jsonString);


		//Use litJson library to parse string
		itemData = JsonMapper.ToObject(jsonString);
		//Debug.Log (itemData ["Weapons"] [1] ["name"]); 			//Example of hardCode use.
		Debug.Log(GetItem("Weapons","Light Rifle")["power"]);		//Another Example
		//Debug.Log(GetString("buttons","play_button")); //doesnt work
	}
	void Update(){}

	/// <summary>
	/// Gets the item by the name "name" in the category "category"
	/// </summary>
	/// <returns>The item in json data form.</returns>
	/// <param name="name">Name.</param>
	/// <param name="category">Type.</param>
	JsonData GetItem(string category,string name){

		for(int i = 0; i<itemData[category].Count;i++){
			if (itemData [category] [i] ["name"].ToString () == name) {
				return itemData [category] [i];
			}
		}
		return null;
	}

	JsonData GetString(string category,string name){
		return itemData [category] [name];
	}
}