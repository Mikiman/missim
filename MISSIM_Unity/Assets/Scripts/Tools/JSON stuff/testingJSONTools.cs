﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class testingJSONTools : MonoBehaviour {

	public ListOfObjects list;

	// Use this for initialization
	void Start () {
		list = new ListOfObjects ();
		save ();

	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void save(){
		//ListOfObjects list = new ListOfObjects ();
		int i = 0;
		myObject o;
		foreach (Transform child in transform) {
			o = new myObject (child.GetSiblingIndex (), child.gameObject.name, child.position, child.rotation.eulerAngles, child.localScale, child);
			list.objects.Add (o);
			i++;
		}

		JSON_Tools.SaveToJSONFile (list, "/Resources/objects.json");
	}

}
[System.Serializable]
public class ListOfObjects{
	public List<myObject> objects; 
	public ListOfObjects(){
		objects = new List<myObject> ();
	}

}

[System.Serializable]
public class myObject{
	public int id;
	public string name;
	public Vector3 position, rotation, scale;
	public Transform transform;

	public myObject(int id,string name,Vector3 position,Vector3 rotation,Vector3 scale,Transform trans){
		this.id = id;
		this.name = name;
		this.position = position;
		this.rotation = rotation;
		this.scale = scale;
		this.transform = trans;
	}

}
