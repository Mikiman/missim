﻿using UnityEngine;
using System.Collections;
using System.IO; 	//for file
using LitJson;		//For Json Parser methods

public static class JSON_Tools{


	/// <summary>
	/// Saves to JSON file.
	/// opens,writes,closes   Overwrites existing file.
	/// </summary>
	/// <param name="jsonData">Json data.</param>
	/// <param name="path">Path.</param>
	public static void SaveToJSONFile(JsonData jsonData,string path){
		File.WriteAllText (Application.dataPath+path,jsonData.ToString()); // opens,writes,closes   Overwrites existing file.
	}

	/// <summary>
	/// Saves ANY class to a JSON file thanks to Generic.
	/// USE:JSON_Tools.SaveToJSONFile(player,"/Resources/playerdata.json");
	/// or: JSON_Tools.SaveToJSONFile<PlayerData>(player,"/Resources/playerdata.json");
	/// 
	/// NOTE: it saves under application.dataPath+path.
	/// </summary>
	/// <param name="myobject">a c# object to save.</param>
	/// <param name="path">Path.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static void SaveToJSONFile<T>(T myobject,string path){
		JsonData jsonData = JsonMapper.ToJson(myobject); 
		SaveToJSONFile (jsonData, path);
	}




	/// <summary>
	/// Loads the JsonData from a file.
	/// opens, reads and closes file
	/// </summary>
	/// <returns>The JSON from file.</returns>
	/// <param name="path">Path.</param>
	public static JsonData LoadJSONFromFile(string path){
		string jsonString = File.ReadAllText(Application.dataPath + path); // opens, reads and closes file

		//Use litJson library to parse string
		return  JsonMapper.ToObject(jsonString);
	}

		

}



