﻿using UnityEngine;
using System.Collections;
using MarkLight;	//for View
using MarkLight.Views.UI;


public class Root : View
{

	public TFG.EditorWindow EdWindow;
	public TFG.MISSIMToolBar ToolBarView;

	public ViewSwitcher ContentViewSwitcher;

	public _string FilePath; //Path del fichero escenario a cargar

	public void ToggleWindow()
	{
		EdWindow.ToggleWindow ();
	}
	public void ToggleToolBarWindow (){
		ToolBarView.ToggleWindow ();
	}


	void Start(){
		EventDemux.instance.RegisterHandler (this);
	}
	void Update(){
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit();
		}
	}



	#region View Switching

	public void ButtonDown_ToCreateMainView(){
		ContentViewSwitcher.SwitchTo (0);
	}
	public void ButtonDown_ToSceneSelectWindow(){
		ContentViewSwitcher.SwitchTo (1);
	}
	public void ButtonDown_ToCreateSceneWindow(){
		ContentViewSwitcher.SwitchTo (2);
	}
	public void ToPlaySceneWindow(){
		ContentViewSwitcher.SwitchTo (3);
	}
	public void ButtonDown_Back()
	{
		ContentViewSwitcher.Previous();
	}
	#endregion


	#region SceneSelectWindow

	public void ButtonDown_EditScene(){
		ButtonDown_ToCreateSceneWindow ();
		EventDemux.instance.ReportEventToDemux(MyEventMeanings.TitleToEdit,FilePath.Value);
	}
	public void ButtonDown_PlayScene(){
		//LoadScene
		ToPlaySceneWindow();
		EventDemux.instance.ReportEventToDemux(MyEventMeanings.TitleToPlay,FilePath.Value);
	}
	#endregion


	public void ButtonDown_BackToEditMode(){
		ButtonDown_ToCreateSceneWindow ();
		EventDemux.instance.ReportEventToDemux (MyEventMeanings.PlayToEdit,"");
	}
	public void ButtonDown_ToPlayMode(){
		/*
		bool saved = EventDemux.instance.editorWindowScript.ClickedButton_Save ();
		if (saved) {
			ToPlaySceneWindow();
			EventDemux.instance.ReportEventToDemux (MyEventMeanings.EditToPlay,"");
		}
		*/
		EventDemux.instance.ReportEventToDemux (MyEventMeanings.EditToPlay, "");

	}

	public void ButtonDown_EditToMainMenu(){
		EventDemux.instance.ReportEventToDemux (MyEventMeanings.DeleteAllAndReturnToMenu,"");
		ButtonDown_ToCreateMainView ();
	}



}