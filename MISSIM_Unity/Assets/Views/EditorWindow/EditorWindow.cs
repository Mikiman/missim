﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MarkLight;	//for View
using MarkLight.Views.UI; //for Window
using TFG.Data;
using ViewModels;


namespace TFG{

public class EditorWindow : View
{

	public Window Window;
	public TabPanel myTabPanel;
	public string SavePath,SaveFileName,FullSavePath;

	//public TaskObjectDataHolder selectedObject=new TaskObjectDataHolder{taskName="Test"};

	//public List<GameObject> ObjectsInScene;
	//public ListMetricViews listGlobalMetrics;

	//public ListTaskObjectDataViews listTaskObjectDataViews;




	public GameObject SelectedObject; //the selected Object in the scene
	public GameObjectInSceneData SelectedObjectData; //Data of the selected Object in scene 
	public TaskObjectDataView taskObjectDataView; //View to pass the data to and call functions
	public SceneDataHolder SceneDataHolderScript; 
	public ListTaskObjects listTaskObjects; //the list of prefabsViews to instantiate TaskObjectViews.


	//Setup to detect setter changes and handle them
	//private GameObject so;
	/*public GameObject SelectedObject{
		get{return SelectedObject;}
		set{
			//SetSelectedObject (value);
			SelectedObject=value;
		}
	}*/





	public void SetSelectedObject(GameObject newSelectedObject){

		SelectedObject = newSelectedObject;

		Debug.Log ("new Gameobject Selected:" +newSelectedObject.name);
		SelectedObjectData =newSelectedObject.GetComponent<GameObjectInSceneDataHolder> ().gameObjectInSceneData; 



		if (taskObjectDataView == null) {
			Debug.Log ("taskObjectDataView is null");
			return;
		}
		else if (SelectedObjectData == null) {
			Debug.Log ("SelectedObject is null");
			return;
			/* } else if (taskObjectDataView.data == null) {
			Debug.Log ("taskObjectDataView.data is null");
			return;*/
		}else{

			Debug.Log ("setting data");
			//FORMA 1
			//taskObjectDataView.SetSelectedGOData (SelectedObjectData);
			taskObjectDataView.SetSelectedGO (newSelectedObject);


			//FORMA 2 el SET NO funciona a solas, necesita llar refresh... =(
			//taskObjectDataView.data = SelectedObjectData;
			//taskObjectDataView.RefreshGOData ();

			Debug.Log ("ended setting data");
		}
		Debug.Log ("Finnehed the SET");
	}




	/*
	private  List<GameObject>  objectsInScene;
	public List<GameObject> ObjectsInScene{
		get
		{
			return objectsInScene;
		}
		set
		{

			objectsInScene = value;

			foreach (GameObject obj in objectsInScene) {

				TaskObjectDataHolder todh= obj.GetComponent<TaskObjectDataHolder> ();
				string taskName = todh;
				//listMetricViews = todh.listMetricViews;
				listTaskObjectDataViews.DataToListView(todh);

			}

		}
	}

*/



	void Start(){
		/*
		for (int i = 0; i < 5; i++) {
			GameObject g = new GameObject("Object "+i);
			ObjectsInScene.Add (g);
		}
		*/

		//_so = new _GameObject ();
		//SelectedObjectData = new TFG.Data._TaskObjectData ();
		//taskObjectDataView = new TaskObjectDataView ();


		SceneDataHolderScript=GameObject.FindGameObjectWithTag("SceneDataHolder").GetComponent<SceneDataHolder>();
		//InvokeRepeating ("SelectNewObject", 0f, 10f);
	}

	private int i;
	private void SelectNewObject(){

		//Chages SelectedObject= ObjectX
		SelectedObject = SceneDataHolderScript.objectsInScene [++i % SceneDataHolderScript.objectsInScene.Length] as GameObject;
		//Debug.Log ("New selectedObjec= "+ SelectedObject.name);


		//CHanges Object0.data = ObjectX.data
		//SelectedObject.GetComponent<GameObjectInSceneDataHolder>().gameObjectInSceneData= (SceneDataHolderScript.objectsInScene [++i % SceneDataHolderScript.objectsInScene.Length]).GetComponent<GameObjectInSceneDataHolder>().gameObjectInSceneData;
		//Debug.Log ("New selectedObject");

	}

	
	public void SetSavePath(SavePathData pathData){
			SavePath = pathData.path;
			SaveFileName = pathData.filename;
			FullSavePath = SavePath + "/" + SaveFileName + ".json";
	}

	public bool ClickedButton_Save(){
		if (SavePath != "" && SaveFileName != "") {
			Debug.Log ("Save Scene");
				FullSavePath = SavePath + "/" + SaveFileName + ".json";
				EventDemux.instance.ReportEventToDemux (MyEventMeanings.Save, FullSavePath);
				return true;
		} else {
			Debug.Log ("Take User To Save Tab to select save path");
			myTabPanel.SelectTab (3, true);
			Window.Open();
			return false;
		}
	}

		public void ClickedButton_Play(){
			EventDemux.instance.ReportEventToDemux (MyEventMeanings.EditToPlay, "");	
		}


	public void ToggleWindow()
	{
		if (Window.IsOpen)
		{
			Window.Close();
		}
		else
		{
			Window.Open();
		}
	}







	/// <summary>
	/// Sets the transform data
	/// Updates 2 things:
	/// -the transform of the gameObject with the given data.
	/// -the GameObjectInSceneData values to save in the json.
	/// </summary>
	/// <param name="data">Data.</param>
	public void SetTransform(MyTransformData data){
		//Debug.Log ("Entered in SetTransform: Position:"+data.position);

		if (SelectedObject == null) {
			Debug.Log ("No SelectedObject");
			return;
		}
		/*
		//UPDATE the Transform of the selectedObject
		SelectedObject.transform.position = data.position;
		SelectedObject.transform.rotation = data.rotation;
		SelectedObject.transform.localScale = data.scale;

		//Update the DATA of the selectedObject
		//TFG.Data.GameObjectInSceneData objectData= //SceneDataHolderScript.objectsInScene [SelectedObjectData.indexInScene].GetComponent<GameObjectInSceneDataHolder> ().gameObjectInSceneData;
		SelectedObjectData.position = data.position;
		SelectedObjectData.rotation = data.rotation;
		SelectedObjectData.scale = data.scale;
		*/

		//Set Todo
		if (data.setAll) {
			//UPDATE the Transform of the selectedObject
			SelectedObject.transform.position = data.position;
			SelectedObject.transform.rotation = data.rotation;
			SelectedObject.transform.localScale = data.scale;

			//Update the DATA of the selectedObject
			SelectedObjectData.position = data.position;
			SelectedObjectData.rotation = data.rotation;
			SelectedObjectData.scale = data.scale;
		} else { //SINO, set solo lo modificado.

			if (data.setPos) {
				SelectedObject.transform.position = data.position;
				SelectedObjectData.position = data.position;
			}

			if (data.setRot) {
				SelectedObject.transform.rotation = data.rotation;
				SelectedObjectData.rotation = data.rotation;
			}

			if (data.setScale) {
				SelectedObject.transform.localScale = data.scale;
				SelectedObjectData.scale = data.scale;
			}

		}

	}

	public override void Initialize(){

			base.Initialize ();

			//Buscar y obtener el SceneDataHolder Object
			SceneDataHolderScript=GameObject.FindGameObjectWithTag("SceneDataHolder").GetComponent<SceneDataHolder>();

			//Dile al EventDemux que yo soy la ventana de Editor
			GameObject.FindGameObjectWithTag("EventDemux").GetComponent<EventDemux>().SetEditorWindowScript(this);



			//int prefabId = 0;




			foreach (GameObject go in SceneDataHolderScript.prefabs) {
				GameObjectInSceneData goisd = go.GetComponent<GameObjectInSceneDataHolder> ().gameObjectInSceneData;
				//goisd.PrefabID = prefabId++; //set and increment


				//TODO ERROR THis code is somehow setting iconPath in data from /Resources/Sprites/icon.png to MarkLight.SpriteAsset causeing an error
				listTaskObjects.AddTaskObjectToList (goisd.taskObjectData); //add it to list to be visible

				Debug.Log ("Should be visible task: "+ goisd.taskObjectData.taskName);
			}

	}


}
}