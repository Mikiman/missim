﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MarkLight.Views.UI;	//for UIView
using MarkLight;//observableList

using ViewModels; //for TaskObjects
using TFG.Data;

namespace ViewModels{
	public class ListTaskObjects : UIView {
		/*
		public ObservableList<ViewModels.TaskObject> TaskObjects;


		public override void Initialize(){
			TaskObjects = new ObservableList<ViewModels.TaskObject> ();

			for (int i = 0; i < 15; i++) {
				//TaskObjects.Add ( new ViewModels.TaskObject{taskName="Name " + i,iconPath="Assets/icon.png"});
				AddTaskObjectToList (new ViewModels.TaskObject{ taskName = "Name " + i, iconPath = "Assets/Resources/Sprites/icon.png" });
			}
			//TaskObjects.ItemsModified ();
		}

		public void ClickedOnTaskObject(){
			var selectedTask = TaskObjects.SelectedItem;


			Debug.Log ("Selected Task:"+ selectedTask.taskName);
		}

		public void AddTaskObjectToList(TaskObject taskObject){
			TaskObjects.Add (taskObject);
			TaskObjects.ItemModified (taskObject);
		}
		*/

		public ObservableList<TaskObjectData> TaskObjects;


		public override void Initialize(){
			TaskObjects = new ObservableList<TaskObjectData> ();
			/*
			for (int i = 0; i < 15; i++) {
				//TaskObjects.Add ( new ViewModels.TaskObject{taskname="Name " + i,iconPath="Assets/icon.png"});
				AddTaskObjectToList (new TaskObjectData{ taskName = "Name " + i, iconPath = "Assets/Resources/Sprites/icon.png" });
			}
			*/
			//TaskObjects.ItemsModified ();
		}

		public void ClickedOnTaskObject(){
			//var selectedTask = TaskObjects.SelectedItem;
			//Debug.Log ("Selected Task:"+ selectedTask.taskName);

			var selectedIndex = TaskObjects.SelectedIndex;
			EventDemux.instance.ReportEventToDemux(MyEventMeanings.AddObjectToSceneByPrefabId,selectedIndex);

		}

		public void AddTaskObjectToList(TaskObjectData taskObject){

			TaskObjects.Add (taskObject.GetCopy()); //Get a copy of the data. dont use the original data
			TaskObjects.ItemModified (taskObject);

		}

	
	
	}
}