﻿using UnityEngine;
using System.Collections;
//using MarkLight;	//for View
using MarkLight.Views.UI;
using TFG.Data;

namespace ViewModels{
	public class TaskObject : ListItem {


		public string taskName;
		public Sprite icon;
		public string iconPath;
		public Color BGC=Color.black;
		public Color IconBGC=Color.cyan;
		public Color TextBGC=Color.red;
		public Color FontCol = Color.white;


		public TaskObjectData data;

		//public Color BackgroundColor;
			
		/*
		public TaskObject(string tasknamel,string iconpathl){
			 taskname.Value = tasknamel;
			iconPath.Value = iconpathl;
		}
		*/


	}
}
