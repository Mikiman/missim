﻿using UnityEngine;
using System.Collections;
using MarkLight;	//for View
using MarkLight.Views.UI;

using TFG.Data; //for MetricViewDataHolder

namespace ViewModels{
	public class MetricView : ListItem {
		/*
		public string metricDescriptionText;
		//public CheckBox metricEnabled;
		public _bool metricEnabled;

		public MetricView(){}

		public MetricView(MetricViewData data){
			metricDescriptionText = data.metricDescriptionText;
			metricEnabled.Value = data.metricEnabled;
		}
		public MetricView(string desc,bool check){
			metricDescriptionText = desc;
			metricEnabled.Value =check;
		}



		public void MyInstantiate (MetricViewData data){
			metricDescriptionText = data.metricDescriptionText;

			//string state=data.metricEnabled?"Checked":"Default";
			//Debug.Log ("state=" + state);

			metricEnabled.Value = data.metricEnabled;
			//Debug.Log ("metricEnabled=" + metricEnabled.Value);

		}

*/

		public MetricViewData data; //TODO ONLY IMPORTANT PART!!! =)
		//public _bool MyActive;     

		public MetricView(){}

		public MetricView(MetricViewData newdata){
			data.metricDescriptionText = newdata.metricDescriptionText;
			data.metricEnabled = newdata.metricEnabled;
		}
		public MetricView(string desc,bool check){
			data.metricDescriptionText = desc;
			data.metricEnabled =check;
		}



		public void MyInstantiate (MetricViewData newdata){

			Debug.Log ("started MyInstantiate: ");
			Debug.Log ("newData: "+newdata.ToString());
			data.metricDescriptionText = newdata.metricDescriptionText;
			Debug.Log ("middle MyInstantiate: ");

			//string state=data.metricEnabled?"Checked":"Default";
			//Debug.Log ("state=" + state);

			data.metricEnabled = newdata.metricEnabled;
			//Debug.Log ("metricEnabled=" + metricEnabled.Value);

		}



	}	
}