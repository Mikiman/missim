﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MarkLight.Views.UI;	//for UIView
using MarkLight;//observableList
using TFG.Data; //for MetricViewDataHolder.cs

using ViewModels; //for MetricView
//using DataClasses;
namespace ViewModels{
	public class ListMetricViews : UIView {
		/*

		public ListMetricViews(){
			//MetricViews = new ObservableList<ViewModels.MetricView> ();
		}

		public ObservableList<ViewModels.MetricView> MetricViews;

	
		public override void Initialize(){
			MetricViews = new ObservableList<ViewModels.MetricView> ();
			/*
			for (int i = 0; i < 10; i++) {
				//TaskObjects.Add ( new ViewModels.TaskObject{taskname="Name " + i,iconPath="Assets/icon.png"});
				AddMetricViewToList (new ViewModels.MetricView{ metricDescriptionText = "Description number " + System.DateTime.Now});
			}
			//TaskObjects.ItemsModified ();

			*/

		/*
	}


	public void MyInitialize(int count){
		//MetricViews = new ObservableList<ViewModels.MetricView> ();

		for (int i = 0; i < count; i++) {
			//TaskObjects.Add ( new ViewModels.TaskObject{taskname="Name " + i,iconPath="Assets/icon.png"});
			AddMetricViewToList (new ViewModels.MetricView{ metricDescriptionText = "Task Metric number " + i});
			Debug.Log ("*************"+ MetricViews[i].metricDescriptionText);
		}

	}




	public void AddMetricViewToList(ViewModels.MetricView metricView){
		if (MetricViews == null) {
			MetricViews = new ObservableList<ViewModels.MetricView> ();
		}
		MetricViews.Add (metricView);
		//Debug.Log("Should be checked="+MetricViews[MetricViews.Count-1].metricEnabled2.Value);//TODO REMOEV
		MetricViews.ItemModified (metricView);

		//Debug.Log ("Added MetricView: "+metricView.ToString());
	}



	/// <summary>
	/// Push data from a data (from DataClasses.MetricViewDataHolder) into the List of ViewModal.MetricViews
	/// </summary>
	/// <param name="dataArray">Data array.</param>
	public void DataArrayToListView(MetricViewData[] dataArray){
		foreach (MetricViewData metricData in dataArray){

			//OJO BAAADDD --> //AddMetricViewToList (new ViewModels.MetricView(metricData.metricDescriptionText,metricData.metricEnabled)); 
			//OJO BAAADDD --> //ViewModels.MetricView view= new ViewModels.MetricView(metricData);

			//Create A new View
			var view=  ViewData.CreateView<ViewModels.MetricView>(this, this,null,"","");
			//Push Data to it
			view.MyInstantiate (metricData); 
			//Add a copy of It to the List
			AddMetricViewToList (view);
			//Destroy the original copy that gets created. (the copy stays in the list).
			view.Destroy (true);

			//Debug.Log ("finished metric view: "+view );
		}
	}




		*/



	/* Segundo Intento

	public ObservableList<ViewModels.MetricView> MetricViews;

	public ListMetricViews(){
		//MetricViews = new ObservableList<ViewModels.MetricView> ();
	}
	public override void Initialize(){
		//MetricViews = new ObservableList<MetricViewData> ();
	}

	public void MyInitialize(int count){
		
		for (int i = 0; i < count; i++) {
			//AddMetricViewToList (new MetricViewData{ metricDescriptionText = new _string{Value="Task Metric number " + i}});
			//Debug.Log ("*************"+ MetricViews[i].metricDescriptionText);
		}

	}




	public void AddMetricViewToList(ViewModels.MetricView metricView){
		if (MetricViews == null) {
			MetricViews = new ObservableList<ViewModels.MetricView> ();
		}
		MetricViews.Add (metricView);
		//Debug.Log("Should be checked="+MetricViews[MetricViews.Count-1].metricEnabled2.Value);//TODO REMOEV
		MetricViews.ItemModified (metricView);

		//Debug.Log ("Added MetricView: "+metricView.ToString());
	}



	/// <summary>
	/// Push data from a data (from DataClasses.MetricViewDataHolder) into the List of ViewModal.MetricViews
	/// </summary>
	/// <param name="dataArray">Data array.</param>
	public void DataArrayToListView(MetricViewData[] dataArray){
		Debug.Log("Dataarry size= "+dataArray.Length);

		foreach (MetricViewData metricData in dataArray){


			//OJO BAAADDD --> //AddMetricViewToList (new ViewModels.MetricView(metricData.metricDescriptionText,metricData.metricEnabled)); 
			//OJO BAAADDD --> //ViewModels.MetricView view= new ViewModels.MetricView(metricData);

			//Create A new View
			var view=  ViewData.CreateView<ViewModels.MetricView>(this, this,null,"","");
			//Push Data to it
			Debug.Log ("metricData= "+metricData.ToString());

			view.MyInstantiate (metricData); 


			Debug.Log ("finished MyInstantiate: ");


			//Add a copy of It to the List
			AddMetricViewToList (view);
			//Destroy the original copy that gets created. (the copy stays in the list).
			view.Destroy (true);

			Debug.Log ("finished metric view: "+view );

		}
	}
	*/



	public ObservableList<MetricViewData> MetricViews;

	public ListMetricViews(){
		MetricViews = new ObservableList<MetricViewData> ();
	}
	public override void Initialize(){
		MetricViews = new ObservableList<MetricViewData> ();

		for (int i = 0; i < 20; i++) {
			AddMetricViewToList (new MetricViewData{MyActive=false});
			//Debug.Log ("*************"+ MetricViews[i].metricDescriptionText);
		}
	}

	public void MyInitialize(int count){

		for (int i = 0; i < count; i++) {
			//AddMetricViewToList (new MetricViewData{ metricDescriptionText = new _string{Value="Task Metric number " + i}});
			//Debug.Log ("*************"+ MetricViews[i].metricDescriptionText);
		}

	}




	public void AddMetricViewToList(MetricViewData metricView){
		System.DateTime startTime = System.DateTime.Now;
		if (MetricViews == null) {
			MetricViews = new ObservableList<MetricViewData> ();
		}
		MetricViews.Add (metricView);
		//Debug.Log("Should be checked="+MetricViews[MetricViews.Count-1].metricEnabled2.Value);//TODO REMOEV
		MetricViews.ItemModified (metricView);

		//Debug.Log ("Added MetricView: "+metricView.ToString()+" Took: " + (System.DateTime.Now-startTime));
	}




	public void Clear(){
			//MetricViews.Clear ();  //TODO this is correct, but to slow... =(

		DataArrayToListView(new MetricViewData[]{});


	}



	/// <summary>
	/// Push data from a data (from DataClasses.MetricViewDataHolder) into the List of ViewModal.MetricViews
	/// </summary>
	/// <param name="dataArray">Data array.</param>
	public void DataArrayToListView(MetricViewData[] dataArray){
		//MetricViews.Clear ();

		/*
		foreach (MetricViewData metricData in dataArray){
			//Add a copy of It to the List
			AddMetricViewToList (metricData);
		}
		*/

		/*
		for (int i = 0; i < dataArray.Length; i++) {
				AddMetricViewToList (dataArray [i]);
		}
		*/



		int d = dataArray.Length; //Number of new data objects
		int l = MetricViews.Count; //number of objects in current list
			
		//Replace old with new
		if (d == l) {
				Debug.Log ("d==1");
				for (int i = 0; i < d; i++) {
				//MetricViews [i].MyActive = false; //apagar old
				MetricViews.Replace (i, dataArray [i]);
				MetricViews [i].MyActive = true;  //encender new

				}
			} else if (d < l) { //Replace the old data with the new, and delete/hide the old leftovers 
			Debug.Log ("d<1");	
			/*	for (int i = 0; i < d; i++) {
					MetricViews.Replace (i, dataArray [i]);
				}
				for (int i = d; i < l; i++) {
					MetricViews.RemoveAt (i);
				}
			*/

			for (int i = 0; i < l; i++) {
				if(i<d){
					//MetricViews [i].MyActive = false; //apagar old
					MetricViews.Replace (i, dataArray [i]);
					MetricViews [i].MyActive = true;
					Debug.Log ("Replaced " + i);
				}else{
					//MetricViews.RemoveAt (i);  //To slow
					if (MetricViews [i].MyActive) { //ONLY have to turn off the ones that are turned on.
						MetricViews.Replace (i, new MetricViewData{ }); //poner uno vacio y apagarlo.  Sin esta linea, modificaba el data del anterior!! 
						MetricViews [i].MyActive = false;
						Debug.Log ("Removed " + i);
					}

				}
			}
				
			}
		else{
			Debug.Log ("d>1");	
				for (int i = 0; i < d; i++) {

					if (i>=l) {
						MetricViews.Add (dataArray [i]);
						Debug.Log ("ADDED " + i);
					} else {
						//MetricViews [i].MyActive = false; //apagar old
						MetricViews.Replace (i, dataArray [i]);
						MetricViews [i].MyActive = true;
						Debug.Log ("Replaced " + i);
					}
				}

		}
	
	}


	}
}


