﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MarkLight.Views.UI;	//for UIView
using MarkLight;

public class SaveScenePanel : UIView {

	[ChangeHandler("Changed_SaveFileName")]
	public _string SaveFileName;
	[ChangeHandler("Changed_SavePath")]
	public _string SavePath;

	public void ButtonDown_BrowseFiles(){
		Debug.Log ("Start Browsing Files");
	}

	public void ButtonDown_CancelSave(){
		Debug.Log ("Cancel Save");
	}
	public void ButtonDown_ConfirmSave(){
		Debug.Log ("Confirm Save");
		EventDemux.instance.ReportEventToDemux (MyEventMeanings.Save,"");// SavePath.Value+ "/" +SaveFileName.Value+ ".json");
	}


	public virtual void Changed_SaveFileName()
	{
		// called once at init and whenever text changes value
		Debug.Log("Changed SaveFileName To:" +SaveFileName);
		EventDemux.instance.ReportEventToDemux (MyEventMeanings.SavePathChanged, new TFG.Data.SavePathData {
			path = SavePath.Value,
			filename = SaveFileName.Value//+".json"
		});

	}
	public virtual void Changed_SavePath()
	{
		// called once at init and whenever text changes value
		Debug.Log("Changed SavePath To:" +SavePath);
		EventDemux.instance.ReportEventToDemux (MyEventMeanings.SavePathChanged, new TFG.Data.SavePathData {
			path = SavePath.Value,
			filename = SaveFileName.Value//+".json"
		});

	}
}
