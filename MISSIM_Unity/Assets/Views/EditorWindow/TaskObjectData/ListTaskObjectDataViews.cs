﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MarkLight.Views.UI;	//for UIView
using MarkLight;//observableList

using ViewModels; //for TaskObjects
using TFG.Data;

namespace ViewModels{
	public class ListTaskObjectDataViews : UIView {

		/*
			public ObservableList<TaskObjectDataView> list;


		public override void Initialize(){
			list = new ObservableList<TaskObjectDataView> ();
		}



		public void ClickedOnTaskObjectData(){
			var selectedTask = list.SelectedItem;


			Debug.Log ("Selected TaskObject:"+ selectedTask.taskName);
			//TODO make Edwindow.selectedObject=selectedTask.gameobject.
		}

		public void AddTaskObjectToList(TaskObjectDataView taskObjectData){
			list.Add (taskObjectData);
			list.ItemModified (taskObjectData);
		}


		/// <summary>
		/// Push data from a data (from DataClasses.MetricViewDataHolder) into the List of ViewModal.MetricViews
		/// </summary>
		/// <param name="dataArray">Data array.</param>
		public void DataArrayToListView(TFG.Data.TaskObjectData[] dataArray){

			if (list == null) {
				list = new ObservableList<TaskObjectDataView> ();
			}

			foreach (TFG.Data.TaskObjectData data in dataArray){
				//Create A new View
				var view=  ViewData.CreateView<TaskObjectDataView>(this, this,null,"","");
				//Push Data to it
				view.MyInstantiate (data); 
				//Add a copy of It to the List
				AddTaskObjectToList (view);
				//Destroy the original copy that gets created. (the copy stays in the list).
				view.Destroy (true);

				//Debug.Log ("finished taskObjectDataView: "+view );
			}
		}

		/// <summary>
		/// Push data from a data (from DataClasses.MetricViewDataHolder) into the List of ViewModal.MetricViews
		/// </summary>
		/// <param name="dataArray">Data array.</param>
		public void DataToListView(TFG.Data.TaskObjectData data){

			if (list == null) {
				list = new ObservableList<TaskObjectDataView> ();
			}


				//Create A new View
				var view=  ViewData.CreateView<TaskObjectDataView>(this, this,null,"","");
				//Push Data to it
				view.MyInstantiate (data); 
				//Add a copy of It to the List
				AddTaskObjectToList (view);
				//Destroy the original copy that gets created. (the copy stays in the list).
				view.Destroy (true);

				//Debug.Log ("finished taskObjectDataView: "+view );

		}
		*/


		public ObservableList<TaskObjectData> list;


		public override void Initialize(){
			list = new ObservableList<TaskObjectData> ();
		}



		public void ClickedOnTaskObjectData(){
			var selectedTask = list.SelectedItem;

			Debug.Log ("Selected TaskObject:"+ selectedTask.taskName);
			//TODO make Edwindow.selectedObject=selectedTask.gameobject.
		}

		public void AddTaskObjectToList(TaskObjectData taskObjectData){
			list.Add (taskObjectData);
			list.ItemModified (taskObjectData);
		}


		/// <summary>
		/// Push data from a data (from DataClasses.MetricViewDataHolder) into the List of ViewModal.MetricViews
		/// </summary>
		/// <param name="dataArray">Data array.</param>
		public void DataArrayToListView(TFG.Data.TaskObjectData[] dataArray){

			if (list == null) {
				list = new ObservableList<TaskObjectData> ();
			}

			list.Clear ();

			foreach (TaskObjectData data in dataArray){
				
				AddTaskObjectToList (data);

			}
		}

	}

}