﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MarkLight.Views.UI;	//for UIView
using MarkLight;//observableList

using ViewModels; //for MetricView
using TFG.Data; //for taskobjectdata;

public class TaskObjectDataView : UIView {


	public _string taskName;
	public ListMetricViews listMetricViews;

	public _Vector3 position;
	public _Vector3 rotation;
	public _Vector3 scale;

	public Slider sliderPosX,sliderPosY,sliderPosZ,sliderRotX,sliderRotY,sliderRotZ,sliderScaleX,sliderScaleY,sliderScaleZ;






	/*
	private TaskObjectData _data;
	public TaskObjectData data{
		get{return _data;}
		set{ 
			_data = value;
			SetSelectedGOData (_data);
		}
	}
	*/

	//[ChangeHandler("RefreshGOData")] //DOESNT EVER GET CALLED... 
	//public GameObjectInSceneData data = new GameObjectInSceneData{};  //TODO GOOD



	public void testFunction(){}


	private GameObject selectedObject;
	public GameObject SelectedObject
	{
		get
		{
			Debug.Log ("Entered GET");
			return selectedObject;

		}
		set
		{
			Debug.Log ("Entered SET");
			SetSelectedGO (value); //TODO

		}
	}

/*
//VERSION 3
	private _GameObject selectedObject;
	public _GameObject SelectedObject
	{
		get
		{
			return selectedObject;
		}
		set
		{
			SetSelectedGO (value.Value); //TODO

		}
	}

	private void SetSelectedGO(GameObject go){
		Debug.Log ("Entered SET: "+go.name);
		selectedObject.Value = go;
		GameObjectInSceneDataHolder goisDH= selectedObject.Value.GetComponent<GameObjectInSceneDataHolder> ();

		SetSelectedGOData(goisDH.gameObjectInSceneData.taskObjectData);

		Debug.Log ("SelectedObject");
	}

	*/






	void OnEnable(){
		//SetSelectedGO (SelectedObject);//TODO
	}



	public void SetSelectedGO(GameObject go){
		Debug.Log ("Entered SET: "+go.name);
		selectedObject = go;
		GameObjectInSceneData goisD = GetData ();;

		//refreshes the slider ui.
		/*
		position.Value = goisDH.gameObjectInSceneData.position;
		rotation.Value = goisDH.gameObjectInSceneData.rotation.eulerAngles;
		scale.Value = goisDH.gameObjectInSceneData.scale;
		*/

		//Refreshes the slider ui, but based on transform instead of json data holder.
		/*
		position.Value = go.transform.position;
		rotation.Value = go.transform.rotation.eulerAngles;
		scale.Value = go.transform.localScale;
		*/



		SetSelectedGOData (goisD);

		//Debug.Log ("SelectedObject");
	}

	public GameObjectInSceneData GetData(){
		return selectedObject.GetComponent<GameObjectInSceneDataHolder> ().gameObjectInSceneData;
	}


	public void SetSelectedGOData(GameObjectInSceneData newdata){
		//Debug.Log ("Entered SET DATA: " +newdata.taskObjectData.taskName);


		//refreshes the slider ui based on json GameObjectInSceneData
		position.Value = newdata.position;
		rotation.Value = newdata.rotation.eulerAngles;
		scale.Value = newdata.scale;


		//fills the metric list
		SetSelectedGOData(newdata.taskObjectData);

		//Debug.Log ("FINISHED SET Data");
	}


	public void SetSelectedGOData(TaskObjectData newdata){
		//Debug.Log ("Entered SET DATA: " +newdata.taskName);

		taskName.Value = newdata.taskName;

		if (listMetricViews == null) {
			listMetricViews = new ListMetricViews ();
		}
		listMetricViews.DataArrayToListView(newdata.metricViewData);
		//Debug.Log ("FINISHED SET Data");
	}




	/*
	/// <summary>
	/// Refreshs the GO data.
	/// SHOULD be called when data is changed, but it isnt called... =(
	/// </summary>
	public virtual void RefreshGOData(){
		
		if (data==null || data == null) {
			Debug.Log ("Entered refresh, but something was null, so exiting");
			return;
		}

		Debug.Log ("Entered refresh DATA: " +data.taskObjectData.taskName);

		taskName.Value = data.taskObjectData.taskName;

		if (listMetricViews == null) {
			Debug.Log ("listMetricViews is null, createng new one");
			listMetricViews = new ListMetricViews ();
		}
		listMetricViews.DataArrayToListView(data.taskObjectData.metricViewData);
		Debug.Log ("FINISHED Refresh Data");

	}
	*/

	/*
	[ChangeHandler("TextChanged")]
	public _string taskName= new _string { Value = "Name" };
	public virtual void TextChanged()
	{
		// called once at init and whenever text changes value
		Debug.Log("New taskName: "+taskName.Value);
	}
	*/




	public void ButtonDown_RemoveTaskObjectFromScene(){
		Debug.Log ("Remove GameobjectFromScene");
		ResetUI ();
		EventDemux.instance.ReportEventToDemux (MyEventMeanings.RemoveObjectFromScene, GetData().indexInScene);
	}

	public void ResetUI(){
		taskName.Value="No selected object.";
		MetricViewData[] empty = new MetricViewData[0];
		listMetricViews.DataArrayToListView(empty);


		position.Value = Vector3.zero;
		rotation.Value = Vector3.zero;
		scale.Value = Vector3.one;

	}



	public override void Initialize(){
		//base.Initialize();

		/**********************************************
		 * 
		 TWO Ways to create a view by code! =)
		 public Group MyGroup;
			a) listMetricViews = ViewData.CreateView<ListMetricViews>(MyGroup, MyGroup,null,"","listMetricViews"); // 'this' is the parent
			b) listMetricViews = this.CreateView(ListMetricViews);
			then call:
				listMetricViews.InitializeViews();
		**********************************************/


		listMetricViews.MyInitialize (0); // NECESARIO ESTA LINEA!!!!!!!
		//data=new _TaskObjectData();
		//listMetricViews.InitializeViews();

		//Debug.Log ("******** Count?" + listMetricViews.MetricViews.Count);
		//Debug.Log ("******** description?" + listMetricViews.MetricViews[1].metricDescriptionText);


	}


	public void MyInstantiate (TFG.Data.TaskObjectData data){
		taskName.Value = data.taskName;
		if (listMetricViews == null) {
			listMetricViews = new ListMetricViews ();
		}
		listMetricViews.DataArrayToListView(data.metricViewData);
	}



	public void ChangePos(){

		Vector3 newpos = new Vector3 (sliderPosX.Value.Value, sliderPosY.Value.Value, sliderPosZ.Value.Value);
		position.Value = newpos;
		//Debug.Log ("entered ChangePos, NewPos= "+ newpos.ToString());
		/*
		 * selectedObject.transform.position = newpos;
		*	EventDemux.instance.ReportEventToDemux (MyEventMeanings.SetTransform, selectedObject.transform);
		**/

		MyTransformData data;
		if (selectedObject == null) {
			data = new MyTransformData{ position = newpos,setPos=true}; 

		} else {
			data = new MyTransformData{position=newpos,rotation=selectedObject.transform.rotation,scale=selectedObject.transform.localScale, setAll=true}; 
		}

		EventDemux.instance.ReportEventToDemux (MyEventMeanings.SetTransform, data);

	}

	public void ChangeRot(){

		Vector3 newrot = new Vector3 (sliderRotX.Value.Value, sliderRotY.Value.Value, sliderRotZ.Value.Value);
		rotation.Value =newrot;
		//Debug.Log ("entered ChangeRot, NewRot "+ newrot.ToString());

		/*selectedObject.transform.rotation = Quaternion.Euler(newrot);
		EventDemux.instance.ReportEventToDemux (MyEventMeanings.SetTransform, selectedObject.transform);
		*/
		//MyTransformData data = new MyTransformData{position=selectedObject.transform.position,rotation=Quaternion.Euler(newrot),scale=selectedObject.transform.localScale}; 


		MyTransformData data;
		if (selectedObject == null) {
			data = new MyTransformData{ rotation =Quaternion.Euler(newrot),setRot=true}; 
		} else {
			data = new MyTransformData{position=selectedObject.transform.position,rotation=Quaternion.Euler(newrot),scale=selectedObject.transform.localScale,setAll=true}; 
		}

		EventDemux.instance.ReportEventToDemux (MyEventMeanings.SetTransform, data);
	}

	public void ChangeScale(){

		Vector3 newscale = new Vector3 (sliderScaleX.Value.Value, sliderScaleY.Value.Value, sliderScaleZ.Value.Value);
		scale.Value = newscale;
		//MyTransformData data = new MyTransformData{position=selectedObject.transform.position,rotation=selectedObject.transform.rotation,scale=newscale}; 


		MyTransformData data;
		if (selectedObject == null) {
			data = new MyTransformData{ scale = newscale,setScale=true}; 
		} else {
			data = new MyTransformData{position=selectedObject.transform.position,rotation=selectedObject.transform.rotation,scale=newscale,setAll=true}; 
		}

		EventDemux.instance.ReportEventToDemux (MyEventMeanings.SetTransform, data);

	}
}
