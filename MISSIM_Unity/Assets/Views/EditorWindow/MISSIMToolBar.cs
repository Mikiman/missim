﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MarkLight;	//for View
using MarkLight.Views.UI; //for Window
using TFG.Data;
using ViewModels;


namespace TFG{

public class MISSIMToolBar : View
{

	public Window Window;
	public TabPanel myTabPanel;
	public string SavePath,SaveFileName;
	public ViewSwitcher ContentViewSwitcher;



	
	
	public int currentToolIndex=0;				//Index of the current tool
	public int[] currentToolModes={0,1,1,1};	//Index of the current mode within the current tool.

	
	//Access to the Frames around the button icons so i can highlight them.
	public Frame ToolFrame0,ToolFrame1,ToolFrame2,ToolFrame3;	//TOOLS
	public Frame ToolModeFrame00, ToolModeFrame01, ToolModeFrame02, ToolModeFrame03,ToolModeFrame04; //Camera
	public Frame ToolModeFrame11, ToolModeFrame15, ToolModeFrame16, ToolModeFrame17; //Position
	public Frame ToolModeFrame21, ToolModeFrame22, ToolModeFrame23, ToolModeFrame24; //Rotation
	public Frame ToolModeFrame31, ToolModeFrame32, ToolModeFrame33, ToolModeFrame34; //Scale

	//A dictionary so i can access the Frames easier.
	Dictionary<string, Frame> buttonFrames =new Dictionary<string, Frame>();
	/// <summary>
	/// Fills the dictionary, associating an id (toolindex+toolmodeIndex) to a button Frame in the xml
	/// </summary>
	void FillButtonFramesDictionary(){
		//TOOLS frames:
		buttonFrames.Add ("0", ToolFrame0);
		buttonFrames.Add ("1", ToolFrame1);
		buttonFrames.Add ("2", ToolFrame2);
		buttonFrames.Add ("3", ToolFrame3);


		//TOOL MODE FRAMES:
		//CAMERA
		buttonFrames.Add ("00", ToolModeFrame00);
		buttonFrames.Add ("01", ToolModeFrame01);
		buttonFrames.Add ("02", ToolModeFrame02);
		buttonFrames.Add ("03", ToolModeFrame03);
		buttonFrames.Add ("04", ToolModeFrame04);

		//Position
		buttonFrames.Add ("11", ToolModeFrame11);
		buttonFrames.Add ("15", ToolModeFrame15);
		buttonFrames.Add ("16", ToolModeFrame16);
		buttonFrames.Add ("17", ToolModeFrame17);

		//Rotation
		buttonFrames.Add ("21", ToolModeFrame21);
		buttonFrames.Add ("22", ToolModeFrame22);
		buttonFrames.Add ("23", ToolModeFrame23);
		buttonFrames.Add ("24", ToolModeFrame24);

		//Scale
		buttonFrames.Add ("31", ToolModeFrame31);
		buttonFrames.Add ("32", ToolModeFrame32);
		buttonFrames.Add ("33", ToolModeFrame33);
		buttonFrames.Add ("34", ToolModeFrame34);
	}

	public Frame getFrameById(string key){
		return buttonFrames.Get (key);
	}



	void Start(){
			SetTool1 (); 	 //Position
			SetToolMode1 (); //XYZ
	}
	

	private int i;
	private void SelectNewObject(){

		//Chages SelectedObject= ObjectX
		//SelectedObject = SceneDataHolderScript.objectsInScene [++i % SceneDataHolderScript.objectsInScene.Length] as GameObject;
		//Debug.Log ("New selectedObjec= "+ SelectedObject.name);


		//CHanges Object0.data = ObjectX.data
		//SelectedObject.GetComponent<GameObjectInSceneDataHolder>().gameObjectInSceneData= (SceneDataHolderScript.objectsInScene [++i % SceneDataHolderScript.objectsInScene.Length]).GetComponent<GameObjectInSceneDataHolder>().gameObjectInSceneData;
		//Debug.Log ("New selectedObject");

	}

	


	public void ClickedButton_Save(){
		if (SavePath != "" && SaveFileName != "") {
			Debug.Log ("Save Scene");
				EventDemux.instance.ReportEventToDemux (MyEventMeanings.Save, SavePath+"/" + SaveFileName);
		} else {
			Debug.Log ("Take User To Save Tab to select save path");
			myTabPanel.SelectTab (3, true);
		}
	}

	public void ToggleWindow()
	{
		if (Window.IsOpen)
		{
			Window.Close();
		}
		else
		{
			Window.Open();
		}
	}



	public void SetTool(int index){
			EventDemux.instance.ReportEventToDemux (MyEventMeanings.SetToolbarTool, index);
			//SetToolMode (1);
			ContentViewSwitcher.SwitchTo (index);


			//Handle selected representation, highlighing the selected tool frame
			getFrameById(currentToolIndex.ToString()).SetState ("Default");
			currentToolIndex = index;
			getFrameById(currentToolIndex.ToString()).SetState ("Selected");

			//Handle the highlighting for the selected tool mode
			SetToolMode (currentToolModes [currentToolIndex]);
	}

	public void SetToolMode(int index){
			EventDemux.instance.ReportEventToDemux (MyEventMeanings.SetToolbarToolMode, index);

			//turn off old frame
			//Debug.Log ("old tool key="+currentToolIndex.ToString()+currentToolModes[currentToolIndex].ToString());
			getFrameById(currentToolIndex.ToString()+currentToolModes[currentToolIndex].ToString()).SetState ("Default");

			//turn on new frame
			currentToolModes [currentToolIndex] = index;
			//Debug.Log ("selected tool key="+currentToolIndex.ToString()+currentToolModes[currentToolIndex].ToString());
			getFrameById(currentToolIndex.ToString()+currentToolModes[currentToolIndex].ToString()).SetState ("Selected");

	}

	public void SetTool0(){SetTool (0);} //USED for camers
	public void SetTool1(){SetTool (1);} //used for pos
	public void SetTool2(){SetTool (2);} //used for rot
	public void SetTool3(){SetTool (3);} //TODO used for scale
	
	public void SetToolMode0(){SetToolMode (0);} //USED for camera default
	public void SetToolMode1(){SetToolMode (1);} //used til 7
	public void SetToolMode2(){SetToolMode (2);}
	public void SetToolMode3(){SetToolMode (3);}
	public void SetToolMode4(){SetToolMode (4);}
	public void SetToolMode5(){SetToolMode (5);}
	public void SetToolMode6(){SetToolMode (6);}
	public void SetToolMode7(){SetToolMode (7);}

	



	public override void Initialize(){

			base.Initialize ();

			//Buscar y obtener el SceneDataHolder Object
			//SceneDataHolderScript=GameObject.FindGameObjectWithTag("SceneDataHolder").GetComponent<SceneDataHolder>();

			//Dile al EventDemux que yo soy la ventana de Editor
			//GameObject.FindGameObjectWithTag("EventDemux").GetComponent<EventDemux>().SetEditorWindowScript(this);


			//IMPORTANT
			FillButtonFramesDictionary ();



	}


}
}