﻿using UnityEngine;
using ViewModels;
using TFG.Data;

[System.Serializable]
public class  GameObjectInSceneDataHolder : MonoBehaviour{

	//OLD:
	public string taskName;
	//public ListMetricViews listMetricViews;
	public MetricViewData[] metricViewData;
	public TFG.Data.TaskObjectData[] taskObjects;

	void Start(){
		//listMetricViews = new ListMetricViews();
		//listMetricViews.MyInitialize (0);
		//listMetricViews.DataArrayToListView (metricViewData);
	}



	//NEW MOST IMPORTANT
	public GameObjectInSceneData gameObjectInSceneData;


	/// <summary>
	/// Passes the data to the GameObjectInSceneData and modifies the Transform
	/// </summary>
	/// <param name="data">Data.</param>
	public void SetUp(GameObjectInSceneData data){
		gameObjectInSceneData = data;

		gameObject.name = data.taskObjectData.taskName;
		transform.position = data.position;
		transform.rotation = data.rotation;
		transform.localScale = data.scale;

		//Debug.Log ("Scale loaded ="+data.scale);

	}

	public GameObjectInSceneData GetUpdatedData(){
		
		/* 
		//get current Transform info
		gameObjectInSceneData.position = transform.position;
		gameObjectInSceneData.rotation = transform.rotation;
		gameObjectInSceneData.scale = transform.localScale;
		*/
		return gameObjectInSceneData;

	}






}