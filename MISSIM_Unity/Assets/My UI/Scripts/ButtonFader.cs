﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonFader : MonoBehaviour {

	public bool fadedIn = false; //this will be used to determine if the next button can fade yet
	public float alphaTreshhold=0.9f; //the next item will fade in when this has reached alpha = thresh. e[0,1]

	Image buttonImage; //to change the alpha color of the image;
	Text txt; //to change the alpha color of the text
	Color buttonColor;
	Color textColor;
	bool startFade = false; //to determine if the button should start fading into the screen
	float smooth =0.05f; //how fast the button fades in
	bool initialized = false; //wheather the proper values have been initialized yet;

	// Use this for initialization
	void Start () {
		Initialize ();
	}


	void Initialize(){
		startFade = false;
		fadedIn = false;
		buttonImage = GetComponent<Image> ();
		buttonColor = buttonImage.color;
		if(GetComponentInChildren<Text>()){ //the button may not have any text
			txt = GetComponentInChildren<Text> ();
			textColor = txt.color;
		}
		initialized = true;
		Mathf.Clamp (alphaTreshhold, 0f, 1f);

	}


	// Update is called once per frame
	void Update () {
		if (startFade) {
			Fade (smooth);
			if(buttonColor.a>alphaTreshhold){
				fadedIn = true;
			}
		}
	}



	public void Fade(float rate){
		//make sure the values have been initialized
		if (!initialized) {
			Initialize ();
		}

		smooth = rate;
		startFade = true;

		//increase the alpha of the components

		buttonColor.a += rate;
		buttonImage.color = buttonColor;

		if (txt) {
			textColor.a += rate;
			txt.color = textColor;
		}


	}
}
