﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;	//for dynamic Lists


public class ButtonBrancher : MonoBehaviour {


	public GameObject[] buttonRefsPrefabs; //Prefabs
	[HideInInspector]
	public List<GameObject> buttons; //additions and removal of dynamic buttons

	//Same as buttonScaler class, used to inicialize it
	public enum ScaleMode{ IndependentWidthHeight, MinimumWidthHeight, MaximumWidthHeight}; 
	public ScaleMode mode;
	public Vector2 referenceButtonSize;
	public Vector2 referenceScreenSize;

	ButtonScaler buttonScaler = new ButtonScaler();
	public RevealSettings revealSettings =new RevealSettings();
	public LinearSpawner linSpawner = new LinearSpawner ();
	public CircularSpawner circSpawner = new CircularSpawner ();

	//Used for detecting if the screen has been resized since last update.
	float lastScreenWidth = 0;
	float lastScreenHeight =0;




	// Use this for initialization
	void Start () {
		buttons = new List<GameObject> ();
		buttonScaler = new ButtonScaler ();
		lastScreenWidth = Screen.width;
		lastScreenHeight =Screen.height;
		buttonScaler.Inicialize (referenceButtonSize, referenceScreenSize, (int)mode);
		circSpawner.FitDistanceToScreenSize (buttonScaler.referenceScreenSize);
		linSpawner.FitSpacingToScreenSize (buttonScaler.referenceScreenSize);

		if (revealSettings.revealOnStart) {
			SpawnButtons ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		UpdateScreenRisizing (); //Changes the size of the buttons if the screen has been resized.
		CallRevealMethods ();	 //Reveals the buttons if they havent been revealed.
	}


	/**
		Changes the size of the buttons if the screen has been resized.
	*/
	private bool UpdateScreenRisizing(){
		//screen has been resized
		if (Screen.width != lastScreenWidth || Screen.height != lastScreenHeight) {
			lastScreenWidth = Screen.width;
			lastScreenHeight = Screen.height;
			buttonScaler.Inicialize (referenceButtonSize, referenceScreenSize, (int)mode);
			circSpawner.FitDistanceToScreenSize (buttonScaler.referenceScreenSize);
			linSpawner.FitSpacingToScreenSize (buttonScaler.referenceScreenSize);
			SpawnButtons ();
			return true;
		}
		//Screen hasnt been resized.
		return false;
	}

	private void CallRevealMethods(){
		if (revealSettings.opening) {
			//Spawn them if they havnt spawned.
			if(!revealSettings.spawned){SpawnButtons();}

			switch (revealSettings.option) {
			case RevealSettings.RevealOption.Linear:
				switch (linSpawner.revealStyle) {
				case LinearSpawner.RevealStyle.SlideToPosition:  RevealLinearlySlideToPosition ();  break;
				case LinearSpawner.RevealStyle.FadeInAtPosition: RevealLinearlyFadeIn ();    break;
				}
				break;

			case RevealSettings.RevealOption.Circular:
				switch (circSpawner.revealStyle) {
				case CircularSpawner.RevealStyle.SlideToPosition:  RevealCircularSlideToPosition ();  break;
				case CircularSpawner.RevealStyle.FadeInAtPosition: RevealCircularFadeIn ();    break;
				}
				break;

			}
		}
	}




	/**
		if the revealOnStart==false, this method will be called by the button click event
	*/
	public void SpawnButtons(){
		revealSettings.opening = true;

		ClearButtonsOnThisLayer ();

		//Create the buttons in the array
		for(int i=0;i<buttonRefsPrefabs.Length;i++){
			GameObject b = Instantiate(buttonRefsPrefabs[i] as GameObject);
			b.transform.SetParent(transform);	//make the button a child of this brancher
			b.transform.position =transform.position; //place the button on the position of the button brancher.

			SetupFadeInIfNecesary (b);

			buttons.Add (b);
		}

		revealSettings.spawned = true;
			
	}




	/*
		Destroyes the button gameobjects of all the buttons in this branch and all the branchers who have the same parent as this one.
		Lets player only see a few buttons at a time
	*/
	private void ClearButtonsOnThisLayer(){
		ClearButtonsOneThisBrancher ();
		ClearButtonsOnBranchersWithSameParent ();
	}


	void ClearButtonsOneThisBrancher(){
		//clear button list in case there are some in it already
		for (int i = buttons.Count - 1; i >= 0; i--) {
			Destroy(buttons[i]);
		}
		buttons.Clear ();
	}
		
	/**
		Clears buttons on any other button brancher that has the same parent as this brancher.
	*/
	void ClearButtonsOnBranchersWithSameParent(){
		GameObject[] branchers = GameObject.FindGameObjectsWithTag ("ButtonBrancher");
		foreach (GameObject brancher in branchers) {
			//check if the parent of this brancher is the same as the parent of the brancher we are looking at
			if (brancher.transform.parent == transform.parent) {
				brancher.GetComponent<ButtonBrancher> ().ClearButtonsOneThisBrancher ();
			}
		}
	}


	/*
		Sets alpha of image and text to 0
		Only works with one text and one image. use array ComponentsInChildren and a foreach loop to set all to zero.
	*/
	private void SetupFadeInIfNecesary(GameObject button){
		bool a = (revealSettings.option==RevealSettings.RevealOption.Linear && linSpawner.revealStyle == LinearSpawner.RevealStyle.FadeInAtPosition);
		bool b = (revealSettings.option==RevealSettings.RevealOption.Circular &&  circSpawner.revealStyle == CircularSpawner.RevealStyle.FadeInAtPosition);

		//if (linSpawner.revealStyle == LinearSpawner.RevealStyle.FadeInAtPosition || circSpawner.revealStyle == CircularSpawner.RevealStyle.FadeInAtPosition) {
		if(a||b){
			//Image transparency to 0
			Color c = button.GetComponent<Image> ().color;
			c.a = 0; //set its transparency to invisible.
			button.GetComponent<Image> ().color = c;

			//Text Transparency to 0
			if(button.GetComponentInChildren<Text>()){ //the button may not have any text
				c = button.GetComponentInChildren<Text> ().color;
				c.a = 0;
				button.GetComponentInChildren<Text> ().color = c;
			}

		}
	}











	/******************************************************************************************************************************************************************************************************************
	 * 		Reveal Methods:
	 *****************************************************************************************************************************************************************************************************************/

	/*
		Slide buttons in a direacion.
	*/
	void RevealLinearlySlideToPosition (){
		for (int i = 0; i < buttons.Count; i++) {
			Vector3 targetPos = GetNextLinearTargetPosition (i);
			RectTransform buttonRect = buttons [i].GetComponent<RectTransform> ();

			buttonRect.position = Vector3.Lerp (buttonRect.position, targetPos, revealSettings.translateSmooth * Time.deltaTime);
		}
	
	}
	/*
	 * Sets the target position of the movement for the linear animation.
	*/
	Vector3 GetNextLinearTargetPosition(int i){
		//give the Button a Position To Move towards:
		Vector3 targetPos;
		RectTransform buttonRect = buttons [i].GetComponent<RectTransform> ();

		//Set size
		buttonRect.sizeDelta= new Vector2(buttonScaler.newButtonSize.x,buttonScaler.newButtonSize.y);

		//Setpos
		targetPos.x= linSpawner.direction.x *((i+linSpawner.buttonNumOffset)*(buttonRect.sizeDelta.x+linSpawner.baseButtonSpacing))+transform.position.x;	//s
		targetPos.y= linSpawner.direction.y *((i+linSpawner.buttonNumOffset)*(buttonRect.sizeDelta.y+linSpawner.baseButtonSpacing))+transform.position.y;
		targetPos.z= 0;

		return targetPos;
	}

	void RevealLinearlyFadeIn(){
		for (int i = 0; i < buttons.Count; i++) {
			//Set up the position to move to
			Vector3 targetPos = GetNextLinearTargetPosition (i);

			FadeInReveal (i,targetPos);

		}
	}


	//Called in fade in reveal methods.
	void FadeInReveal(int i,Vector3 targetPos){
		ButtonFader previousButtonFader, buttonFader=null;

		if (i > 0) {
			previousButtonFader = buttons [i - 1].GetComponent<ButtonFader> ();
		} else {
			previousButtonFader = null;
		}

		buttonFader = buttons [i].GetComponent<ButtonFader> ();


		//the first button wont have a previous button
		if (previousButtonFader) {
			if (previousButtonFader.fadedIn) {
				buttons [i].transform.position = targetPos;
				if (buttonFader) {
					buttonFader.Fade (revealSettings.fadeSmooth);
				} else {
					Debug.Log ("You want to fade your buttons but they need a button fader script attached");
				}
			}
		} else { //for the first button
			buttons [i].transform.position = targetPos;
			if (buttonFader) {
				buttonFader.Fade (revealSettings.fadeSmooth);
			} else {
				Debug.Log ("You want to fade your buttons but they need a button fader script attached to the first button");
			}
		}
	}


	void RevealCircularSlideToPosition(){
		for (int i = 0; i < buttons.Count; i++) {

			Vector3 targetPos = GetNextCircularTargetPosition (i);

			RectTransform buttonRect = buttons[i].GetComponent<RectTransform>();

			//Resize the button
			buttonRect.sizeDelta = new Vector2 (buttonScaler.newButtonSize.x, buttonScaler.newButtonSize.y);

			buttonRect.position = Vector3.Lerp (buttonRect.position,targetPos,revealSettings.translateSmooth*Time.deltaTime);

		}
	}

	Vector3 RotateAroundPivot(Vector3 point, Vector3 pivot,float angle){
		Vector3 targetPoint = point - pivot;						//Gives me the vector from center of circle to point in the circle
		targetPoint = Quaternion.Euler (0, 0, angle) * targetPoint;//Quaternion * Vector3 = Vector3 with angle of the quaternion
		targetPoint += pivot;
		return targetPoint;
	}

	Vector3 GetNextCircularTargetPosition(int i){
		//Find Angle: Create buttons in arc from min to max angle = alpha.
		float angleDist = circSpawner.angle.maxAngle-circSpawner.angle.minAngle;
		float targetAngle = circSpawner.angle.minAngle + (angleDist / buttons.Count) * i; //beta = min + delta*i

		//find Pos
		Vector3 targetPos = transform.position + Vector3.right*circSpawner.distanceFromBrancher;
		targetPos = RotateAroundPivot (targetPos, transform.position,targetAngle);
		return targetPos;
	}


	void RevealCircularFadeIn(){
		for (int i = 0; i < buttons.Count; i++) {
			
			Vector3 targetPos = GetNextCircularTargetPosition (i);
		
			RectTransform buttonRect = buttons [i].GetComponent<RectTransform> ();

			//Resize the button
			buttonRect.sizeDelta = new Vector2 (buttonScaler.newButtonSize.x, buttonScaler.newButtonSize.y);

			FadeInReveal (i, targetPos);
		}
	}







	/******************************************************************************************************************************************************************************************************************
	 * 		4 Clases Auxiliares
	 *****************************************************************************************************************************************************************************************************************/
	public class ButtonScaler{
		enum ScaleMode{ IndependentWidthHeight, MinimumWidthHeight, MaximumWidthHeight}	//Square and Circles use match. rectangular use independents
		ScaleMode mode;
		Vector2 referenceButtonSize;

		[HideInInspector]
		public Vector2 referenceScreenSize; //same as canvas screensize generally;
		public Vector2 newButtonSize; //Scale buttons to this size.


		//Similar to a Constructor
		public void Inicialize(Vector2 refButtonSize,Vector2 refScreenSize,int scaleMode){
			mode = (ScaleMode)scaleMode; //use a cast to pass the int
			referenceButtonSize = refButtonSize;
			referenceScreenSize = refScreenSize;
			SetNewButtonSize ();
		}

		void SetNewButtonSize(){
			if (mode == ScaleMode.IndependentWidthHeight) {
				newButtonSize.x = (referenceButtonSize.x * Screen.width) / referenceScreenSize.x;
				newButtonSize.y = (referenceButtonSize.y * Screen.height) / referenceScreenSize.y;
			} else if (mode == ScaleMode.MinimumWidthHeight) {
				float x = (referenceButtonSize.x * Screen.width) / referenceScreenSize.x;
				float y = (referenceButtonSize.y * Screen.height) / referenceScreenSize.y;
				newButtonSize.x = newButtonSize.y = Mathf.Min (x,y);
			}
			else if (mode == ScaleMode.MaximumWidthHeight) {
				float x = (referenceButtonSize.x * Screen.width) / referenceScreenSize.x;
				float y = (referenceButtonSize.y * Screen.height) / referenceScreenSize.y;
				newButtonSize.x = newButtonSize.y = Mathf.Max (x,y);
			}
		}
	}

	[System.Serializable]
	public class RevealSettings
	{
		public enum RevealOption{Linear,Circular};
		public RevealOption option;
		[Tooltip("How fast the buttons move to their positions")]
		public float translateSmooth = 5f;

		[Tooltip("How fast the buttons fide in (if they fade)")]
		public float fadeSmooth = 0.01f;

		public bool revealOnStart; //mainly used only for first screen on loadup

		[HideInInspector]
		public bool opening= false;
		//[HideInInspector]
		public bool spawned= false;

	}

	[System.Serializable]
	public class LinearSpawner{
		public enum RevealStyle {SlideToPosition,FadeInAtPosition};
		public RevealStyle revealStyle;
		public Vector2 direction = new Vector2 (0, 1); //slide down.  (0,0) is top left corner.
		public float baseButtonSpacing = 5f; //how much space between each button, remember, the sizes depend on the screen size. This helos keep it looking good.
		public int buttonNumOffset = 0; //how many button spaces offset? sometimes necesary when using multiple button branches.

		[HideInInspector]
		public float buttonSpacing = 5f; //this value changes based on the baseButtonSpacing;

		public void FitSpacingToScreenSize(Vector2 refScreenSize){
			float refScreenAverage = (refScreenSize.x + refScreenSize.y) / 2;
			float screenAverage = (Screen.width + Screen.height) / 2;
			buttonSpacing = (baseButtonSpacing * screenAverage) / refScreenAverage;
		}
	}

	[System.Serializable]
	public class CircularSpawner{
		public enum RevealStyle {SlideToPosition,FadeInAtPosition};
		public RevealStyle revealStyle;
		public Angle angle;
		public float baseDistanceFromBrancher = 20f; //radius of the circle containing the button.

		[HideInInspector]
		public float distanceFromBrancher = 0; //radius of the circle based on screen size.

		[System.Serializable]
		public struct Angle {public float minAngle;public float maxAngle;}

		public void FitDistanceToScreenSize(Vector2 refScreenSize){
			float refScreenAverage = (refScreenSize.x + refScreenSize.y) / 2;
			float screenAverage = (Screen.width + Screen.height) / 2;
			distanceFromBrancher = (baseDistanceFromBrancher * screenAverage) / refScreenAverage;
		}
	}






















}
