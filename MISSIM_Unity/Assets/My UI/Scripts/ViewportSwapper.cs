﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ViewportSwapper : MonoBehaviour {
	public Text[] viewPortTexts;
	public string[] viewPortTitles;
	public Camera[] cameras;
	public Rect[] cameraRectSizes;

	//For setup2
	public bool initializeTransform=true;
	public Vector3[] positions,rotations;




	//public MyViewPort[] viewPorts;

	// Use this for initialization
	void Start () {
		//InitializeCameraRects ();
		if(initializeTransform){InitializeCameraTransforms();}
	}
	

	public void Swap(int index){
		//Swap order of cameras in array;
		Camera temp = cameras[0];
		cameras[0]=cameras[index];
		cameras[index] =temp;

		//Edit their Rect sizes
		UpdateCameraRects();

		//Swap the values of the texts;
		string tempString = viewPortTexts [0].text;
		viewPortTexts [0].text = viewPortTexts [index].text;
		viewPortTexts [index].text = tempString;

	}

	public void UpdateCameraRects(){
		for (int i = 0; i < cameraRectSizes.Length; i++) {
			cameras[i].rect = cameraRectSizes [i];
		}
	}

	public void InitializeCameraRects(){
		//inicialize the values to mathc the game object values.
		for (int i = 0; i < cameraRectSizes.Length; i++) {
			cameraRectSizes [i] = cameras[i].rect;
			viewPortTexts [i].text = viewPortTitles [i];
		}
	}

	public void InitializeCameraTransforms(){
		Transform t;
		for (int i = 0; i < cameras.Length; i++) {
			t = cameras[i].transform;

			//Unparent, position, reparent Fixes positioning for all screen sizes.
			t.parent = null;
			t.position = positions [i];
			t.rotation = Quaternion.Euler (rotations [i]);
			t.parent = transform;

		}
	}


	public void Swap2(int index){
		//Swap order of cameras in array;
		Camera temp = cameras[0];
		cameras[0]=cameras[index];
		cameras[index] =temp;

		//Edit their Rect sizes
		//UpdateCameraRects();
		cameras [0].gameObject.SetActive (true);
		cameras[0].rect = new Rect (0.4f,0,0.6f,0.85f);

		cameras [index].gameObject.SetActive (false);
		//cameras[index].rect =new Rect(0,0,0,0);



		//Swap the values of the texts;
		string tempString = viewPortTexts [0].text;
		viewPortTexts [0].text = viewPortTexts [index].text;
		viewPortTexts [index].text = tempString;

	}



}
