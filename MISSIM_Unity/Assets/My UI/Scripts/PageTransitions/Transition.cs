﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


/// <summary>
/// This script should be placed on all the pages that transition.
/// </summary>
public class Transition : MonoBehaviour {

	public enum OutTransitionType {Fade,FadeInstant,Flicker};
	public OutTransitionType outTransitionType=OutTransitionType.Fade;
	public enum InTransitionType {Fade,FadeInstant,Flicker};
	public InTransitionType inTransitionType=InTransitionType.Fade;

	//the need to be children of a canvas to be seen
	public string parentTag;
	public Vector3 spawnPosition = Vector3.zero;

	public float fadeSpeed=2;
	public float flickerRate = 0.025f;



	//Private variables to handle bringing in the new page;
	bool transitionInitialized = false; 	//for safty, dont do anyhting if it hasnt bee initialized yet
	bool startTransition=false;				//Modified by the UI_MenuController
	float inColorAlpha = 0;
	float outColorAlpha = 0;
	Text[] transitionTexts,currentTexts;	//so i can turn on and off all the images and texts in the currentPage and the transitionPage
	Image[] transitionImages,currentImages;
	RectTransform transitionPage,thisPage;








	// Use this for initialization
	void Start () {
		//if out transition is set to fade (FADE OUT) we need alpha to be one.
		if (outTransitionType == OutTransitionType.Fade) {
			outColorAlpha = 1;
		}

		thisPage = GetComponent<RectTransform> ();

		//find all the texts and images to fade or flicker
		currentImages = GetComponentsInChildren<Image> ();
		currentTexts = GetComponentsInChildren<Text> ();

	
	}
	
	// Update is called once per frame
	void Update () {
		if (startTransition) {
			switch (outTransitionType) { //page leaving;
				case OutTransitionType.Fade:		FadePageOut ();	break;
				case OutTransitionType.FadeInstant:	outColorAlpha = 0;break;
				case OutTransitionType.Flicker:		StartCoroutine ("FlickerOut", flickerRate);break;
			}

			switch (inTransitionType) { //new page comming in;
				case InTransitionType.Fade:		FadePageIn ();	break;
				case InTransitionType.FadeInstant:	outColorAlpha = 1;break;
				case InTransitionType.Flicker:		StartCoroutine ("FlickerIn", flickerRate);break;
			}


			UpdateTransitionPageColors ();
			UpdateCurrentPageColors ();
		}
	}



	/******************************************************************************************************************************************************************************************************************
	 * 	 Public Methods:
	 *****************************************************************************************************************************************************************************************************************/



	/// <summary>
	/// Initializes the transition page that is comming in.
	/// </summary>
	/// <returns>The transition page.</returns>
	/// <param name="transitionPage">Transition page.</param>
	public GameObject InitializeTransitionPage (GameObject transition){
		//Set the transition page
		GameObject go = Instantiate(transition as GameObject);
		transitionPage = go.GetComponent<RectTransform> ();

		//the transition page parent needs to be the canvas (or one of the children of the canvas)
		transitionPage.transform.SetParent(GameObject.FindGameObjectWithTag(parentTag).transform);

		//Keep the correct scale
		transitionPage.transform.localScale=Vector3.one; 

		//find all the texts and images to fade or flicker
		transitionImages = GetComponentsInChildren<Image> ();
		transitionTexts = GetComponentsInChildren<Text> ();

		//start the page off at transparent
		foreach (Text t in transitionTexts) {
			t.color = new Vector4 (t.color.r, t.color.g, t.color.b, 0);
		}

		foreach (Image i in transitionImages) {
			i.color = new Vector4 (i.color.r, i.color.g, i.color.b, 0);
		}

		transitionInitialized = true;

		return transitionPage.gameObject;


	}




	/// <summary>
	/// Sets the starttransition to true.
	/// Called by the UI_MenuConroller;
	/// </summary>
	public void StartTransition(){
		startTransition = true;
	}










	/******************************************************************************************************************************************************************************************************************
	 * 	 Private Methods:
	 *****************************************************************************************************************************************************************************************************************/


	//Out Transition Methods

	/// <summary>
	/// Fades the page out.
	/// </summary>
	void FadePageOut(){
		outColorAlpha = Mathf.Lerp (outColorAlpha, 0, fadeSpeed * Time.deltaTime);
	}
	
	IEnumerator FlickerOut(float frecuency){
		for (int i = 0; i < 8; i++) {
			yield return new WaitForSeconds (frecuency);
			outColorAlpha = 0.35f;
			yield return new WaitForSeconds (frecuency);
			outColorAlpha = 0.8f;
		}
	}


	//In Transition Methods
	void FadePageIn(){
		//lerp gets slower the closer it gets to destination
		inColorAlpha = Mathf.Lerp (inColorAlpha, 1, fadeSpeed * Time.deltaTime);

		if (inColorAlpha > 0.99f) {
			inColorAlpha = 1; //the object wont be destroyed untl alpha ==1;
		}
		if (inColorAlpha == 1) {
			//new page is loaded in now so we can destroy this page
			Destroy (gameObject);
		}
	}

	IEnumerator FlickerIn(float frecuency){
		for (int i = 0; i < 8; i++) {
			yield return new WaitForSeconds (frecuency);
			inColorAlpha = 0.35f;
			yield return new WaitForSeconds (frecuency);
			inColorAlpha = 1.0f;
		}
		//this will happen after the for loop;
		//if (inColorAlpha = 1.0f) {
			Destroy (gameObject);
		//}
	}




	void UpdateTransitionPageColors()
	{
		if (transitionImages != null) {
			foreach (Image i in transitionImages) {
				i.color = new Vector4 (i.color.r, i.color.g, i.color.b, inColorAlpha);
			}
		}

		if(transitionTexts!=null){
			foreach (Text t in transitionTexts) {
				t.color = new Vector4 (t.color.r, t.color.g, t.color.b, inColorAlpha);
			}
		}

	}



	void UpdateCurrentPageColors()
	{
		if (currentImages != null) {
			foreach (Image i in currentImages) {
				i.color = new Vector4 (i.color.r, i.color.g, i.color.b, outColorAlpha);
			}
		}

		if(currentTexts!=null){
			foreach (Text t in currentTexts) {
				t.color = new Vector4 (t.color.r, t.color.g, t.color.b, outColorAlpha);
			}
		}

	}

				
















}
