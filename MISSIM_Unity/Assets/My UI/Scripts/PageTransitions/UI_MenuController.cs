﻿using UnityEngine;
using System.Collections;

public class UI_MenuController : MonoBehaviour {

	///<summary>
	/// This controller will be responsible for determining which pages are being loaded. 
	/// </summary>

	[Tooltip("Add all the pages that can be transitioned to from your menu.")]
	public GameObject[] pagesPrefabs;
	[Tooltip("These are the names tat arecodes to set each page. REMEMBER: same order as the pages prefabs.")]
	public string[] pageNames;

	GameObject currentPage;

	//the enter screen bool is for pause or  other in game menus
	bool enterScreen=false;

	public bool EnterScreen
	{
		get {return enterScreen;}
		set {enterScreen = value;}
	}




	// Use this for initialization
	void Start () {
		SetCurrentPage(pagesPrefabs[0]);
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	/// <summary>
	/// Sets the current page.
	/// Note: it s only used to set the first menu page
	/// </summary>
	/// <param name="page">pagesPrefab[0].</param>
	void SetCurrentPage(GameObject page){
		GameObject p = Instantiate (page as GameObject);
		p.transform.SetParent (transform); //if it isnt a child of the canvas it wont be seen.
		RectTransform rt = p.GetComponent<RectTransform>();
		Transition t = p.GetComponent<Transition> ();

		//offset max and min because canvas uses alt+shift+strech anchors.
		//how much space between the anchor and the page.
		rt.offsetMax = new Vector2 (t.spawnPosition.x, t.spawnPosition.y);
		rt.offsetMin = new Vector2 (t.spawnPosition.x, t.spawnPosition.y);

		//Fixes a rescale bug in unity to maintain the correct scale.
		p.transform.localScale = Vector3.one;

		currentPage = p;

	}






	/// <summary>
	/// This method is called when you click on a nav button in the menu.
	/// </summary>
	/// <param name="Page_CODE">Page COD.</param>
	public void SetNextPage(string PAGE_CODE){
		for (int i = 0; i < pageNames.Length; i++) {
			if (PAGE_CODE == pageNames [i]) {
				//only works if the pages are in the same order as the pagenames!!!
				RevealPageInUI (i);
			}
		}
	}

	void RevealPageInUI(int index){
		Transition t = currentPage.GetComponent<Transition> ();

		//this will start the transition out of the current page
		t.StartTransition();

		//Set the current page to the page that is coming in (Initialize will spawn and return the next page)
		currentPage= t.InitializeTransitionPage(pagesPrefabs[index]);

		//position the page based on its offset
		RectTransform rt = currentPage.GetComponent<RectTransform>();
		t = currentPage.GetComponent<Transition> ();
		rt.offsetMax = new Vector2 (t.spawnPosition.x, t.spawnPosition.y);
		rt.offsetMin = new Vector2 (t.spawnPosition.x, t.spawnPosition.y);
	}


}
