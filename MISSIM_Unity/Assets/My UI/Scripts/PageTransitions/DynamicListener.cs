﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class DynamicListener : MonoBehaviour {

	public string tagOfObjectoToListenTo = "Enter GameObjects Tag";
	public bool listenToSelf = false;	
	public bool parameter = false;   //true if the method called onButtonDown requires a parameter		
	public string sendMessage = "enter GameObjects Method name";  //method to call onClick of button
	public string messageParameter;	//

	//the button component on this gameobject
	Button button;
	GameObject listeningToGameobject_X;

	// Use this for initialization
	void Start () {
		button = GetComponent<Button> ();
		GetObjectToListenTo ();
	}

	void GetObjectToListenTo(){
		//if we are listenting to this gameobject
		if (listenToSelf) {
			listeningToGameobject_X = gameObject;
		} else {//if listening to another game object
			listeningToGameobject_X = GameObject.FindGameObjectWithTag(tagOfObjectoToListenTo);
		}

		if (listeningToGameobject_X) {
			SetListener ();
		}
			
	}

	/// <summary>
	/// Sets the listener.
	/// Creates the listener on the button.
	/// If there isnt a button component on the gameobject, this will throw an error.
	/// </summary>
	void SetListener(){
		//if there is indeed a button component on this gameobject
		if (button) {
			if (parameter) {
				button.onClick.AddListener (() => listeningToGameobject_X.SendMessage (sendMessage, messageParameter));
			} else {
				button.onClick.AddListener (() => listeningToGameobject_X.SendMessage (sendMessage));
			}
		} else {
			Debug.LogError ("Dynamic Listeners Script can only be placed on a gameobject that has a button component");
		}
	}

}
